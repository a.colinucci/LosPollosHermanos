$(function() {

  $("#sidebar-icon").click(function() {
    $('.ui.labeled.icon.sidebar').sidebar('toggle');
  })

  $(".select-sezioni").dropdown();

  $("#aggiungi-sezione").click(function() {
    $("#menu-container").append(
      "<div class='fields'>" +
        "<div class='required field'>" +
          "<label>Sezione</label>" +
          "<input type='text' placeholder='Sezione' name='sezioni[]'>" +
        "</div>" +
        "<div class='field'>" +
          "<label>Descrizione</label>" +
          "<textarea rows='2' placeholder='Descrizione' name='descrizioni[]' style='width: 300px'></textarea>" +
        "</div>"
    );
  })

})
