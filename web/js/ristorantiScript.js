$(function() {

  $('.special.cards .image').dimmer({
    on: 'hover'
  });

  $("#new-restaurant-icon").click(function() {
    $("#new-restaurant-modal").modal('setting', 'transition', 'vertical flip').modal('show');
  })

})
