$(function() {

  $(".listino-icon").click(function() {
    var id = $(this).attr("id").substring(19);
    $("#listino-prezzi-modal" + id).modal('setting', 'transition', 'vertical flip').modal('show');
  })

  $(".select-product").click(function(e) {

    if (this.checked) {

      $("#prezzo-totale-fornitura").text("0 €");

      console.log($(e.target));
      var p_id = $(this).attr("data-id");
      var r_id = $("#richiedi-fornitura-form").attr("data-id");

      $.ajax({
        type: "POST",
        url: "/fornitori/prodotto/" + r_id + "/" + p_id,
        beforeSend: function() {
          $("#fornitori-forniture").empty();
          $('.loader').show();
        },
        complete: function(){
           $('.loader').hide();
        },
        success: function(response) {

          if (response.fornitori.length == 0) {
            $("#snackbar").text("Nessun risultato");

            $("#snackbar").addClass("show");
            setTimeout(function() {
              $("#snackbar").removeClass("show");
            }, 2000);
          } else {

            for (i = 0; i < response.fornitori.length; i++) {

              $("#fornitori-forniture").append(
                "<div class='field' style='margin-bottom: 10px'>" +
                  "<div class='ui tag labels' style='display: inline'>" +
                    "<a id='prezzo" + response.fornitori[i].id + "' class='ui yellow label'>€ " + response.fornitori[i].prezzo + "</a>" +
                  "</div>" +
                  "<div class='ui toggle checkbox'>" +
                    "<input class='radio-fornitore' type='radio' name='fornitore' value=" + response.fornitori[i].id + " required>" +
                    "<label style='display: inline'>" + response.fornitori[i].nomeAzienda + "</label>" +
                  "</div>" +
                "</div>"
              );

            }

          }
        }

      })
    }

  })

  $("#quantita-prodotto").click(function(e) {

    e.preventDefault();

    if ($("input[name='fornitore']:checked").val()) {
      var id = ($("input[name='fornitore']:checked").attr("value"));
      var prezzo = parseFloat($("#prezzo" + id).text().substring(2));
      console.log(prezzo);
      $("#prezzo-totale-fornitura").text(prezzo * $(this).val() + " €");
      $("#totale").val(prezzo * $(this).val());
    }

  })

});
