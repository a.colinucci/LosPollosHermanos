$(function() {

  $(".add-pietanza").click(function() {

    var id = $(this).attr("data-id");

    if ($("#menu").find("form.active").length == 0) {
      $("#sezione" + id).find("form").slideDown();
      $("#form" + id).addClass("active");
    } else {
      $("#snackbar").text("Puoi aggiungere 1 solo piatto alla volta");

      $("#snackbar").addClass("show");
      setTimeout(function() {
        $("#snackbar").removeClass("show");
      }, 2000);
    }
  })

  $('body').on('click', '#cancel-new-pietanza', function() {
    $(this).parents().eq("2").fadeOut();
    $(this).parents().eq("2").removeClass("active");
  });

  $(".pietanzaPrezzo").click(function() {

    var nome = $(this).attr("data-name");
    var id = $(this).attr("data-id");
    var prezzo = $(this).text();
    var found = false;

    $("#ordine tr").each(function() {
      if($(this).find(".nome").text() == nome) {
        found = true;
      }
    })

    if (!found) {
      $("#ordine").append(
        "<tr>" +
          "<td class='nome' data-id='" + id + "'>" + nome + "</td>" +
          "<td>" +
            "<input class='quantita' min='1' value='1' type='number' style='width: 100%; border: 0; text-align: center; outline: 0' name='quantita[]' />" +
          "</td>" +
          "<td class='prezzo'>" + prezzo + "</td>" +
          "<td nowrap>" +
            "<i class='deleteFromOrder trash icon' style= 'cursor: pointer' title='Elimina'></i>" +
          "</td>" +
        "</tr>"
      );


      var newPrice = parseInt($("#prezzo-totale").text().slice(0, -1)) + parseInt(prezzo.slice(0, -1));
      $("#prezzo-totale").text(newPrice + " €");

    } else {
      $("#snackbar").text("La pietanza è già nel carrello.");

      $("#snackbar").addClass("show");
      setTimeout(function() {
        $("#snackbar").removeClass("show");
      }, 2000);
    }

    if ($("#ordine").find("tr").length > 0) {
      $("#checkout-button").fadeIn();
    }

  })

  $('body').on('click', '.quantita', function(event) {

    var newPrice = 0;

    $("#ordine tr").each(function() {
      newPrice += $(this).find("input").val() * (parseInt($(this).find(".prezzo").text().slice(0, -1)));
    })

    $("#prezzo-totale").text(newPrice + " €");
  });

  $('body').on('click', '.deleteFromOrder', function(event) {

    var quantita = $(event.target).parent().prev().prev().find("input").val();
    console.log(quantita);
    var prezzo = parseInt($(event.target).parent().prev().text().slice(0, -1));
    console.log(prezzo);
    var newPrice = parseInt($("#prezzo-totale").text().slice(0, -1)) - (quantita * prezzo);

    $("#prezzo-totale").text(newPrice + " €");

    $(event.target).parent().parent().remove();

    if ($("#ordine").find("tr").length == 0) {
      $("#checkout-button").fadeOut();
    }
  });

  var totale = 0;

  $('body').on('click', '#checkout-button', function(event) {

    if ($(this).attr("data-id") == "true") {

      $("#checkout-cart").empty();

      $("#ordine tr").each(function() {

        $("#checkout-cart").append(
          "<tr>" +
            "<td>" + $(this).find(".nome").text() + "</td>" +
            "<input type='hidden' name='pietanze[]' value='" + $(this).find('.nome').attr('data-id') + "'/>" +
            "<td>" + $(this).find(".quantita").val() + "</td>" +
            "<input type='hidden' name='quantita[]' value='" + $(this).find('.quantita').val() + "'/>" +
            "<td>" + $(this).find(".prezzo").text() + "</td>" +
          "</tr>"
        );

      })

      $("#checkout-totale").text($("#prezzo-totale").text());

      totale = parseInt($("#prezzo-totale").text().slice(0, -1));

      $("#checkout-modal").modal('setting', 'transition', 'vertical flip').modal('show');

    } else {
      $("#snackbar").text("Il ristorante oggi è chiuso");

      $("#snackbar").addClass("show");
      setTimeout(function() {
        $("#snackbar").removeClass("show");
      }, 2000);
    }
  })

  $(".cap-selection").change(function() {
    var supplemento = parseInt($(this).find(":selected").text().substr($(this).find(":selected").text().length -1));
    $("#checkout-totale").text(totale + supplemento + " €");
    $("#checkout-input-totale").val(totale + supplemento);
  })

  $("#check-disponibilita").click(function() {

    var r_id = $(this).attr("data-id");

    if ($("#orario-consegna").val() == "") {

      $("#snackbar").text("Scegliere un orario prima di verificare");

      $("#snackbar").addClass("show");
      setTimeout(function() {
        $("#snackbar").removeClass("show");
      }, 2000);

    } else {

      if ($("#checkout-submit-button").attr("data-name") == "checked" ) {
        $("#snackbar").text("La disponibilità è già stata confermata");

        $("#snackbar").addClass("show");
        setTimeout(function() {
          $("#snackbar").removeClass("show");
        }, 2000);

      } else {

        var data = $("#orario-consegna").attr("data-name");
        var ora = $("#orario-consegna").val();

        console.log(data);
        console.log(ora);

        $.ajax({
          type: "POST",
          url: "/user/ristorante/" + r_id + "/check/orari/",
          data: {
            "date": data,
            "orario": ora
          },
          beforeSend: function() {
            $("#check-disponibilita").addClass("loading");
          },
          complete: function(){
            $("#check-disponibilita").removeClass("loading");
          },
          success: function(response) {

            if (response) {
              $("#check-disponibilita").addClass("green");
              $("#check-disponibilita").css("padding-left", "0");
              $("#check-disponibilita").text("Disponibilità confermata");
              $("#checkout-submit-button").attr("data-name", "checked");
              $("#orario-consegna").attr("readonly", "true");
            } else {
              $("#check-disponibilita").css("padding-left", "0");
              $("#check-disponibilita").text("L' orario inserito non coincide con quelli del ristorante.");
            }

          }

        })
      }

    }

  })

  $('#checkout-form').submit(function() {

    if ($("#checkout-submit-button").attr("data-name") == "unchecked") {

      $("#snackbar").text("Prima di confermare l'ordine verificare l'orario di consegna");

      $("#snackbar").addClass("show");
      setTimeout(function() {
        $("#snackbar").removeClass("show");
      }, 2000);

      return false;
    } else {

      $("#checkout-submit-button").addClass("loading");
    }

  })

})
