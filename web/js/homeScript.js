$(function() {

  $("h1").transition('jiggle');

  $("#admin-button, #user-button").click(function() {
    $(this).addClass("loading");
  })

  $("form").submit(function() {
    $(".submit-button").addClass("loading");
  })

  $("#sign-up-button").click(function() {
    $("#new-user-modal").modal('setting', 'transition', 'vertical flip').modal('show');
  })

  $('input[name="confirm-password"]').keyup(function() {
    if ($(this).val() == $('input[name="password"]').val()) {
      $('input[name="password"]').parent().removeClass("error").addClass("success");
      $(this).parent().removeClass("error").addClass("success");
    } else {
      $('input[name="password"]').parent().addClass("error");
      $(this).parent().addClass("error");
    }
  })

  $('input[name="confirm-password-fornitore"]').keyup(function() {
    if ($(this).val() == $('input[name="password-fornitore"]').val()) {
      $('input[name="password-fornitore"]').parent().removeClass("error").addClass("success");
      $(this).parent().removeClass("error").addClass("success");
    } else {
      $('input[name="password-fornitore"]').parent().addClass("error");
      $(this).parent().addClass("error");
    }
  })

  $('#radio-user').change(function() {
    if ($(this).is(':checked')) {
      $("input[name='user']").val(true);
      $('#new-supplier-form').fadeOut(function() {
        $('#new-user-form').fadeIn();
      });
    } else {
      $("input[name='user']").val(false);
      $('#new-user-form').fadeOut();
    }
  })

  $('#radio-supplier').change(function() {
    if ($(this).is(':checked')) {
      $("input[name='supplier']").val(true);
      $('#new-user-form').fadeOut(function () {
        $('#new-supplier-form').fadeIn();
      });
    } else {
      $("input[name='supplier']").val(false);
      $('#new-supplier-form').fadeOut();
    }
  })

})
