$(function() {

  $('.message .close').on('click', function() {
    $(this).closest('.message').transition('fade');
  });

  $("#user-icon").popup({
    inline   : true,
    hoverable: true,
    postition: 'bottom',
    transition: 'slide down',
    delay: {
      show: 200,
      hide: 200
     }
  });

  $(".fly").transition('fly right');

  $('.tabular.menu .item').tab();

  $(".ui.dropdown").dropdown();
  
})
