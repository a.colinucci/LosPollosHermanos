
$(function() {

  $("#seleziona-giorno-prenotazione").datepicker({

      onSelect: function(formattedDate, date, inst) {

        var today = new Date();
        var dateTmp = new Date(date);
        dateTmp.setHours(23);

        if (today > dateTmp) {

          $("#snackbar").text("Non è una macchina del tempo!");
          $("#snackbar").addClass("show");
          inst.clear();
          setTimeout(function() {
            $("#snackbar").removeClass("show");
          }, 2000);

        }

      }

  })

  var orario = "";
  var numeroTavolo = 0;
  var persone = 0;
  var data = "";
  var ristorante_id = 0;

  $("#verifica-disponibilita-button").click(function() {

    orario = $('input[name="orario"]').val();
    persone = $('input[name="persone"]').val();
    data = $("#seleziona-giorno-prenotazione").val();
    ristorante_id = $("#prenotazione-ristorante").val();

    $.ajax({
      type: "POST",
      url: "/user/ristorante/" + ristorante_id + "/check/prenotazione/",
      data: {
        "date": data,
        "orario": orario,
        "persone": persone
      },
      beforeSend: function() {
        $("#verifica-disponibilita-box").empty();
        $("#verifica-disponibilita-button").addClass("loading");
      },
      complete: function(){
        $("#verifica-disponibilita-button").removeClass("loading");
      },
      success: function(response) {

        var found = -1;

        if (typeof response == "string") {
          $("#verifica-disponibilita-box").append(
            "<h2 class='ui center aligned icon header'>" +
              "<i class='circular remove red icon'></i>" +
            "</h2>" +
            "<p style='text-align: center; margin-top: 20px'>" + response + "</p>"
          );
        } else {
          $("#verifica-disponibilita-box").append(
            "<h2 class='ui center aligned icon header'>" +
              "<i class='circular checkmark green icon'></i>" +
            "</h2>" +
            "<p style='text-align: center; margin-top: 20px'>Il ristorante conferma la sua disponibilità " +
             "in data <strong>" + data + "</strong> alle ore <strong>" + orario + "</strong> per <strong>" + persone + " persone</strong>. <br>Puoi proseguire con la prenotazione.</p>" +
             "<div id='prenota-button' style='margin: 20px 0 0 45%' class='ui animated positive button' tabindex='0'>" +
               "<div class='visible content'>Prenota</div>" +
               "<div class='hidden content'>" +
                 "<i class='right arrow icon'></i>" +
               "</div>" +
             "</div>"
          );

          for (i = 0; i < response.tavoli.length; i++) {
            if (response.tavoli[i].posti == persone) {
                found = i;
            }
          }

          if (found != -1) {
            numeroTavolo = response.tavoli[found].numero;
          } else {
            numeroTavolo = response.tavoli[0].numero;
          }
        }
      }
    })
  })

  $('body').on('click', '#prenota-button', function() {

      $.ajax({
        type: "POST",
        url: "/user/ristorante/" + ristorante_id + "/prenotazione/" + numeroTavolo + "/",
        data: {
          "date": data,
          "orario": orario,
          "persone": persone
        },
        beforeSend: function() {
          $("#prenota-button").addClass("loading");
        },
        complete: function(){
          $("#prenota-button").removeClass("loading");
        },
        success: function(response) {

          $("#verifica-disponibilita-box").empty();

          $("#verifica-disponibilita-box").append(
            "<div class='ui success message'>" +
              "<div class='header'>" +
                "Conferma prenotazione" +
              "</div>" +
              "<p>" + response + "</p>" +
            "</div>"
          );

        }
      })

  })

})
