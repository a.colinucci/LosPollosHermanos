$(function() {

  $("#seleziona-turno").datepicker({

      onSelect: function(formattedDate, date, inst) {

        inst.hide();

        var r_id = $("#turni").attr("data-id");

        $.ajax({
          type: "POST",
          url: "/admin/ristorante/" + r_id + "/check/turni/",
          data: {
            "date": formattedDate
          },
          beforeSend: function() {
            $("#turni").contents(':not(.loader)').remove();
            $('.loader').show();
          },
          complete: function(){
            $('.loader').hide();
          },
          success: function(response) {
            if (response == false) {
              $("#snackbar").text("Il ristorante è chiuso nel giorno selezionato!");

              $("#snackbar").addClass("show");
              setTimeout(function() {
                $("#snackbar").removeClass("show");
              }, 2000);
            } else if (response.turni.length == 0) {
              $("#snackbar").text("Non sono previsti turni per questo giorno");

              $("#snackbar").addClass("show");
              setTimeout(function() {
                $("#snackbar").removeClass("show");
              }, 2000);
            } else {
              console.log(response);
              $("#turni").append(
                "<table class='ui celled table' style='margin-left: 2.5%'>" +
                  "<thead>" +
                    "<tr>" +
                      "<th>Ruolo</th>" +
                      "<th>Nome</th>" +
                      "<th>Cognome</th>" +
                      "<th>Inizio turno</th>" +
                      "<th>Fine turno</th>" +
                    "</tr>" +
                  "</thead>" +
                  "<tbody>" +
                  "</tbody>" +
                "</table>"

              );

              var i = 0;

              for (i = 0; i < response.turni.length; i++) {
                $("#turni tbody").append(
                  "<tr>" +
                    "<td>" +
                      "<div class='ui ribbon label'>" + response.turni[i].dipendente.ruolo + "</div>" +
                    "</td>" +
                    "<td>" + response.turni[i].dipendente.nome + "</td>" +
                    "<td>" + response.turni[i].dipendente.cognome + "</td>" +
                    "<td>" + response.turni[i].ora_inizio.substring(11, 16) + "</td>" +
                    "<td>" + response.turni[i].ora_fine.substring(11, 16) + "</td>" +
                  "</tr>"
                );
              }
            }
          }

        })

      }

    })

    $("#add-to-dipendenti").click(function() {
      $("#new-dipendente-modal").modal('setting', 'transition', 'vertical flip').modal('show');
    })

    $("#add-turno").click(function() {
      $("#new-turno-modal").modal('setting', 'transition', 'vertical flip').modal('show');
    })

    $("#check-turno").click(function() {

      var r_id = $(this).attr("data-id");

      if ($("input[name='giorno']").val() == "" ||
          $("input[name='fascia']").val() == "" ||
          $("input[name='dipendente']").val() == "" ||
          $("input[name='inizio-turno']").val() == "" ||
          $("input[name='fine-turno']").val() == ""
         ) {

        $("#snackbar").text("Prima di verificare compilare i parametri");

        $("#snackbar").addClass("show");
        setTimeout(function() {
          $("#snackbar").removeClass("show");
        }, 2000);

      } else {

        if ($("#submit-turno").attr("data-name") == "checked" ) {
          $("#snackbar").text("La disponibilità è già stata confermata");

          $("#snackbar").addClass("show");
          setTimeout(function() {
            $("#snackbar").removeClass("show");
          }, 2000);

        } else {

          var data = $("input[name='giorno']").val();
          var codDipendente = $("#dipendente option:selected").val();
          var fascia = $("#fascia option:selected").val();

          console.log(data);
          console.log(codDipendente);
          console.log(fascia);

          $.ajax({
            type: "POST",
            url: "/admin/ristorante/" + r_id + "/check/turno/",
            data: {
              "date": data,
              "codDipendente": codDipendente,
              "fascia": fascia
            },
            beforeSend: function() {
              $("#check-turno").addClass("loading");
            },
            complete: function(){
              $("#check-turno").removeClass("loading");
            },
            success: function(response) {
              if (!response) {
                $("#submit-turno").attr("data-name", "checked");
                $("#check-turno").removeClass("primary").addClass("positive");
                $("#check-turno").text("Turno confermato!");
              } else {
                $("#check-turno").text("Il dipendente selezionato ha già un turno in quella fascia oraria. Prova ancora!");
              }
            }

          })
        }

      }

    })

    $('#new-turno-form').submit(function() {

      if ($("#submit-turno").attr("data-name") == "unchecked") {

        $("#snackbar").text("Prima di confermare il turno verificare i parametri");

        $("#snackbar").addClass("show");
        setTimeout(function() {
          $("#snackbar").removeClass("show");
        }, 2000);

        return false;
      } else {
        $("#submit-turno").addClass("loading");
      }

    })

})
