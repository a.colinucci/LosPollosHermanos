$(function() {

  $("#aggiungi-prodotti-magazzino-form").submit(function() {
    $(".checkbox-prodotti:checked").each(function(e) {
      var quantita = $(this).parents().eq(1).nextAll().eq(3).html();
      $("#aggiungi-prodotti-magazzino-form").append("<input type='hidden' value='" + quantita + "' name='quantita[]'/>");
    })
    e.preventDefault();
  })

  $(".checkbox-prodotti").change(function() {
    if ($(this).is(':checked')) {
      $(this).val($(this).attr("data-id"));
    } else {
      $(this).removeAttr("value");
    }
  })

  $(".edit-product").click(function() {
    var id = $(this).attr("id").substring(27);
    console.log("edit-product-magazzino-modal" + id);
    $("#edit-product-magazzino-modal" + id).modal('show');
  });

})
