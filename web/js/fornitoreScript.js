$(function() {

  $("#aggiungi-prodotti-listino-form").submit(function() {
    $(".checkbox-prodotti:checked").each(function(e) {
      var prezzo = $(this).parents().eq(1).nextAll().eq(3).html();
      $("#aggiungi-prodotti-listino-form").append("<input type='hidden' value='" + prezzo + "' name='prezzi[]'/>");
    })
    e.preventDefault();
  })

  $(".checkbox-prodotti").change(function() {
    if ($(this).is(':checked')) {
      $(this).val($(this).attr("data-id"));
    } else {
      $(this).removeAttr("value");
    }
  })

  $("#disponibilita-ristoranti-form").submit(function() {
    $("#disponibilità").val("1");
    $(".checkbox-ristoranti:checked").each(function(e) {
      var id = $(this).attr("data-id");
      $("#disponibilita-ristoranti-form").append("<input type='hidden' value='" + id + "' name='ristoranti[]'/>");
    })
    e.preventDefault();
  })

  $("#edit-prodotto-listino-form").submit(function() {
    $("#edit-prodotto").val("1");
  })

  $(".edit-product").click(function() {
    var id = $(this).attr("id").substring(17);
    $("#edit-product-modal" + id).modal('show');
  });

})
