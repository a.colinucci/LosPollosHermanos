$(function() {

  $(".recensione-button").click(function() {
    $("input[name='ordine']").val($(this).attr("data-id"));
    $("input[name='ristorante']").val($(this).attr("data-name"));
    $("#new-recensione-modal").modal('setting', 'transition', 'vertical flip').modal('show');
  })

  $('.ui.rating').rating();

  $('.ui.rating').click(function() {
    $("input[name='voto']").val($("#assegna-punteggio").rating('get rating'));
  })

  $('.voto-recensione').rating('disable');

})
