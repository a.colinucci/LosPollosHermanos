
$(function() {

  $("#new-orari-button").click(function() {
    $("#new-orari-modal").modal('setting', 'transition', 'vertical flip').modal('show');
  })

  $("#seleziona-giorno").datepicker({

      onSelect: function(formattedDate, date, inst) {

        $("#giorno-apertura").val(formattedDate);

        inst.hide();

        $.ajax({
          type: "POST",
          url: "/admin/ristorante/" + $("#orari").attr("data-id") + "/check/orari/",
          data: {
            "date": formattedDate
          },
          beforeSend: function() {
            $("#orari").empty();
            $('.loader').show();
          },
          complete: function(){
            $('.loader').hide();
          },
          success: function(response) {

            if (response.giornoApertura == null) {

              var today = new Date();
              var dateTmp = new Date(date);
              dateTmp.setHours(23);

              if (today > dateTmp) {
                $("#snackbar").text("Il ristorante è stato chiuso nel giorno selezionato!");

                $("#snackbar").addClass("show");
                setTimeout(function() {
                  $("#snackbar").removeClass("show");
                }, 2000);
              } else {
                $("#orari-response-modal").modal("show");
              }
            } else {
              $("#orari").append(
                "<div class='ui bulleted list' style='margin-top: 20px'>" +
                  "<div class='item'>" +
                    "<div><strong>Pranzo</strong></div>" +
                    "<div class='list'>" +
                      "<div class='item'>Apertura: " + (response.giornoApertura.orario_apertura_pranzo == null ? "-" : response.giornoApertura.orario_apertura_pranzo.substring(11, 16)) + "</div>" +
                      "<div class='item'>Chiusura: " + (response.giornoApertura.orario_chiusura_pranzo == null ? "-" : response.giornoApertura.orario_chiusura_pranzo.substring(11, 16)) + "</div>" +
                    "</div>" +
                  "</div>" +
                  "<div class='item'>" +
                    "<div><strong>Cena</strong></div>" +
                    "<div class='list'>" +
                      "<div class='item'>Apertura: " + response.giornoApertura.orario_apertura_cena.substring(11, 16) + "</div>" +
                      "<div class='item'>Chiusura: " + response.giornoApertura.orario_chiusura_cena.substring(11, 16) + "</div>" +
                    "</div>" +
                  "</div>" +
                "</div>"
              );

            }
          }
        })
      }

  })

})
