<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class FornitoriController extends Controller
{

  /**
   * @Route("/fornitore/home/{id}", name="home_fornitore")
   */
  public function homeAction($id, Request $request) {

    $fornitore = $this->getDoctrine()
                       ->getRepository("AppBundle:Fornitore")
                       ->find($id);

    $ristoranti = $this->getDoctrine()
                       ->getRepository("AppBundle:Ristorante")
                       ->findAll();

    if (!$this->get('security.authorization_checker')->isGranted('ROLE_FORNITORE')) {
        return $this->redirectToRoute('fos_user_security_login');
    }

    if ($request->getMethod() === Request::METHOD_POST) {

      $em = $this->getDoctrine()->getManager();

      if ($request->request->get('disponibilità') == "1") {

        $ristorantiIds = $request->request->get('ristoranti', []);

        $fornitore->clearRistoranti();

        foreach ($ristorantiIds as $id) {
        $ristorante = $this->getDoctrine()
                           ->getRepository("AppBundle:Ristorante")
                           ->find($id);

        $fornitore->addRistoranti($ristorante);

          $em->persist($fornitore);
        }

        $em->flush();

        $this->addFlash(
          "success",
          "Disponibilità confermate!"
        );
      } else {

        $p_id = $request->request->get('prodotto');
        $f_id = $request->request->get('fornitore');
        $prezzo = $request->request->get('prezzo');

        $prodottoInListino = $em->getRepository("AppBundle:ProdottoInListino")
                                ->findOneBy(array(
                                  'fornitore' => $f_id,
                                  'prodotto' => $p_id
                                ));

        $prodottoInListino->setPrezzo($prezzo);

        $em->persist($prodottoInListino);
        $em->flush();

        $this->addFlash(
          "success",
          "Prodotto modficato con successo!"
        );
      }

      return $this->redirectToRoute("home_fornitore", array("id" => $fornitore->getId()));
    }

    return $this->render('view/fornitori/fornitore_home.html.twig', array(
      'fornitore' => $fornitore,
      'ristoranti' => $ristoranti
    ));
  }


  /**
   * @Route("/fornitore/{id}/listino/", name="fornitore_aggiungi_prodotto_listino")
   */
  public function newProductToListinoAction($id, Request $request) {

    $fornitore = $this->getDoctrine()
                       ->getRepository("AppBundle:Fornitore")
                       ->find($id);

    $prodotti = $this->getDoctrine()
                     ->getRepository("AppBundle:Prodotto")
                     ->findAll();

    if (!$this->get('security.authorization_checker')->isGranted('ROLE_FORNITORE')) {
        return $this->redirectToRoute('fos_user_security_login');
    }

    if ($request->getMethod() === Request::METHOD_POST) {

      $em = $this->getDoctrine()->getManager();

      $productIds = $request->request->get('prodotti', []);
      $prezzi = $request->request->get('prezzi', []);

      foreach ($productIds as $key=>$id) {

        $prodottoInListino = new \AppBundle\Entity\ProdottoInListino();

        $prodotto = $this->getDoctrine()
                         ->getRepository("AppBundle:Prodotto")
                         ->find($id);

        $prodottoInListino->setPrezzo($prezzi[$key] ? $prezzi[$key] : null);
        $prodottoInListino->setProdotto($prodotto);
        $prodottoInListino->setFornitore($fornitore);

        $em->persist($prodottoInListino);

      }

      $em->flush();

      $this->addFlash(
        "success",
        count($productIds) . " prodotti aggiunti a listino!"
      );

      return $this->redirectToRoute("home_fornitore", array("id" => $fornitore->getId()));
    }

    return $this->render('view/fornitori/fornitore_new_product_to_listino.html.twig', array(
      'fornitore' => $fornitore,
      'prodotti' => $prodotti
    ));
  }

  /**
   * @Route("/fornitore/listino/elimina/{f_id}/{p_id}", name="elimina_prodotto_listino")
   */
  public function deleteProductAction($f_id, $p_id)  {

      $em = $this->getDoctrine()->getManager();
      $prodottoInListino = $em->getRepository("AppBundle:ProdottoInListino")
                              ->findOneBy(array(
                                'fornitore' => $f_id,
                                'prodotto' => $p_id
                              ));

      $em->remove($prodottoInListino);
      $em->flush();

      $this->addFlash(
        "success",
        "Prodotto rimosso dal listino!"
      );

      return $this->redirectToRoute("home_fornitore", array("id" => $prodottoInListino->getFornitore()->getId()));
  }

  /**
   *
   *@Route("/fornitori/prodotto/{r_id}/{p_id}", name="fornitori_prodotti_check")
   */
   public function checkProductAction($r_id, $p_id)  {

    $sql = "SELECT nomeAzienda, prezzo, id
            FROM prodotto_in_listino
            JOIN fornitore ON (prodotto_in_listino.fornitore_id = fornitore.id)
            WHERE prodotto_in_listino.prodotto_id = $p_id
            AND prodotto_in_listino.fornitore_id IN (SELECT fornitore_ristorante.fornitore_id
                                                    FROM fornitore_ristorante
                                                    WHERE fornitore_ristorante.ristorante_id = $r_id)";

    $stmt = $this->getDoctrine()->getManager()->getConnection()->prepare($sql);
    $stmt->execute();
    $result = $stmt->fetchAll();

    $serializer = $this->container->get('jms_serializer');
    $fornitori = $serializer->serialize(array("fornitori" => $result), 'json');
    $response = new Response($fornitori);
    $response->headers->set("Content-Type", "application/json");

    return $response;
   }

}
