<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class HomeController extends Controller
{

    /**
     * @Route("/", name="homepage")
     */
    public function homeAction(Request $request) {

      if ($request->getMethod() === Request::METHOD_POST) {


        $em = $this->getDoctrine()->getManager();

        $user = new \AppBundle\Entity\User();

        $user->setEmail($request->request->get('email'));
        $user->setUsername($request->request->get('username'));
        $user->setEnabled(true);

        if ($request->request->get('user') == "true") {

          $user->setPlainPassword($request->request->get('password'));

          $cliente = new \AppBundle\Entity\Cliente();

          $cliente->setNome($request->request->get('nome'));
          $cliente->setCognome($request->request->get('cognome'));
          $cliente->setIndirizzo($request->request->get('indirizzo'));
          $cliente->setCitta($request->request->get('citta'));
          $cliente->setCellulare($request->request->get('phone'));
          $cliente->setNumeroCarta($request->request->get('numero-carta'));
          $cliente->setCvvCarta($request->request->get('cvv'));
          $cliente->setMeseScadenzaCarta($request->request->get('scadenza-mese'));
          $cliente->setAnnoScadenzaCarta($request->request->get('scadenza-anno'));

          $user->setRoles(array('ROLE_USER'));

          $em->persist($cliente);

          $user->setCliente($cliente);

          $em->persist($user);

        } else {

          $user->setPlainPassword($request->request->get('password-fornitore'));

          $fornitore = new \AppBundle\Entity\Fornitore();

          $fornitore->setNomeAzienda($request->request->get('nome_azienda'));
          $fornitore->setPartitaIVA($request->request->get('partita_iva'));
          $fornitore->setIndirizzo($request->request->get('indirizzo'));
          $fornitore->setCitta($request->request->get('citta'));
          $fornitore->setCellulare($request->request->get('phone'));
          $user->setRoles(array('ROLE_FORNITORE'));

          $em->persist($fornitore);

          $user->setFornitore($fornitore);

          $em->persist($user);
        }

        $em->flush();

        $this->addFlash(
          "success",
          "User added successfully!"
        );

        return $this->redirectToRoute("fos_user_security_login");
      }

      return $this->render('view/home/home.html.twig');
    }

    /**
     * @Route("/select/role", name="role_selection")
     */
    public function roleAction() {

      if ($this->get('security.authorization_checker')->isGranted('ROLE_FORNITORE')) {

        $id = $user = $this->get('security.token_storage')->getToken()->getUser()->getFornitore()->getId();

        return $this->redirectToRoute("home_fornitore", array("id" => $id));
      } else {
        return $this->redirectToRoute("user_ristoranti_lista");
      }

    }

}
