<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Filesystem\Filesystem;

class RistorantiController extends Controller {
    /**
     * @Route("/admin/ristoranti/", name="admin_ristoranti_lista")
     */
    public function adminListAction(Request $request) {

      $ristoranti = $this->getDoctrine()
                         ->getRepository("AppBundle:Ristorante")
                         ->findAll();

      if ($request->getMethod() === Request::METHOD_POST) {

        $em = $this->getDoctrine()->getManager();

        $ristorante = new \AppBundle\Entity\Ristorante();

        $ristorante->setNome($request->request->get('nome'));
        $ristorante->setIndirizzo($request->request->get('indirizzo'));
        $ristorante->setCitta($request->request->get('citta'));
        $ristorante->setTelefono($request->request->get('telefono'));
        $ristorante->setMaxCoperti(0);

        $em->persist($ristorante);
        $em->flush();

        $this->addFlash(
          "success",
          "Nuovo ristorante aggiunto!"
        );

        return $this->redirectToRoute("admin_ristoranti_lista");
      }

      return $this->render('view/ristoranti/ristoranti_lista.html.twig', array(
        'ristoranti' => $ristoranti
      ));
    }

    /**
     * @Route("/admin/ristoranti/prodotti", name="admin_ristoranti_lista_prodotti")
     */
    public function adminProductsListAction(Request $request) {

      $prodotti = $this->getDoctrine()
                       ->getRepository("AppBundle:Prodotto")
                       ->findAll();

      if ($request->getMethod() === Request::METHOD_POST)  {

          $em = $this->getDoctrine()->getManager();

          $prodotto = new \AppBundle\Entity\Prodotto();

          $prodotto->setNome($request->request->get('nome'));
          $prodotto->setDescrizione($request->request->get('descrizione'));
          $prodotto->setTipo($request->request->get('tipo'));

          $em->persist($prodotto);
            //Controllare se il prodotto è un oggetto o un ingrediente
          $em->flush();

          $this->addFlash(
            "success",
            "Nuovo prodotto aggiunto!"
          );

          return $this->redirectToRoute("admin_ristoranti_lista_prodotti");
      }

      return $this->render('view/ristoranti/ristoranti_lista_prodotti.html.twig', array(
        'prodotti' => $prodotti
      ));
    }

    /**
     * @Route("/user/ristoranti/", name="user_ristoranti_lista")
     */
    public function userListAction() {

      $ristoranti = $this->getDoctrine()
                         ->getRepository("AppBundle:Ristorante")
                         ->findAll();

      if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
          return $this->redirectToRoute('fos_user_security_login');
      } else {
        return $this->render('view/ristoranti/ristoranti_lista.html.twig', array(
            'ristoranti' => $ristoranti
        ));
      }
    }

    /**
     * @Route("/user/ristoranti/{id}", name="user_ristorante_dettagli")
     */
    public function userDetailsAction($id)  {

      $ristorante = $this->getDoctrine()
                         ->getRepository("AppBundle:Ristorante")
                         ->find($id);

      return $this->render('view/ristoranti/user_ristorante_dettagli.html.twig', array(
          'ristorante' => $ristorante
      ));
    }

    /**
     * @Route("/admin/ristoranti/{id}", name="admin_ristorante_dettagli")
     */
    public function adminDetailsAction($id)  {

      $sql = "SELECT p.orario, p.data, p.persone, p.numeroTavolo, c.nome
              FROM prenotazione p, cliente c
              WHERE p.codRistorante = $id
              AND p.data > CURRENT_TIME
              ORDER BY p.data, p.orario ASC";

      $stmt = $this->getDoctrine()->getManager()->getConnection()->prepare($sql);
      $stmt->execute();
      $prenotazioni = $stmt->fetchAll();

      $ristorante = $this->getDoctrine()
                         ->getRepository("AppBundle:Ristorante")
                         ->find($id);


      return $this->render('view/ristoranti/admin_ristorante_dettagli.html.twig', array(
          'ristorante' => $ristorante,
          'prenotazioni' => $prenotazioni
      ));
    }

    /**
     * @Route("/admin/ristorante/menu/{id}", name="admin_menu")
     */
    public function adminMenuAction($id, Request $request) {

      $prodotti = $this->getDoctrine()
                       ->getRepository("AppBundle:Prodotto")
                       ->findAll();

      $ristorante = $this->getDoctrine()
                         ->getRepository("AppBundle:Ristorante")
                         ->find($id);

      if ($request->getMethod() === Request::METHOD_POST) {

        $em = $this->getDoctrine()->getManager();

        $pietanza = new \AppBundle\Entity\Pietanza();

        $sezione = $this->getDoctrine()
                        ->getRepository("AppBundle:Sezione")
                        ->find($request->request->get('sezione'));

        $ingredientiIds = $request->request->get('ingredienti', []);

        foreach ($ingredientiIds as $id) {

          $ingrediente = $this->getDoctrine()
                              ->getRepository("AppBundle:Prodotto")
                              ->find($id);

          $pietanza->addIngredienti($ingrediente);
        }

        $pietanza->setSezione($sezione);
        $pietanza->setNome($request->request->get('nome'));
        $pietanza->setDescrizione($request->request->get('descrizione'));
        $pietanza->setPrezzo($request->request->get('prezzo'));
        $pietanza->setCalorie($request->request->get('calorie'));
        $pietanza->setGrassi($request->request->get('grassi'));
        $pietanza->setCarboidrati($request->request->get('carboidrati'));
        $pietanza->setZuccheri($request->request->get('zuccheri'));
        $pietanza->setSale($request->request->get('sale'));
        $pietanza->setProteine($request->request->get('proteine'));

        $em->persist($pietanza);
        $em->flush();

        $this->addFlash(
          "success",
          "Nuova pietanza aggiunta!"
        );

        return $this->redirectToRoute("admin_menu", array('id' => $ristorante->getId()));
      }

      return $this->render('view/menu/admin_menu_dettagli.html.twig', array(
          'ristorante' => $ristorante,
          'prodotti' => $prodotti
      ));

    }

    /**
     * @Route("/user/ristorante/menu/{id}", name="user_menu")
     */
    public function userMenuAction($id, Request $request) {

      $ristorante = $this->getDoctrine()
                         ->getRepository("AppBundle:Ristorante")
                         ->find($id);


      if ($request->getMethod() === Request::METHOD_POST) {

        $em = $this->getDoctrine()->getManager();

        $ordine = new \AppBundle\Entity\Ordine();

        $cap = $request->request->get('cap');

        $tariffa = $this->getDoctrine()
                        ->getRepository("AppBundle:Tariffario")
                        ->findOneBy(array(
                          "cap" => $cap,
                          "ristorante" => $id
                        ));

        $ordine->setOraConsegna(\DateTime::createFromFormat('H:i', $request->request->get('orario-consegna')));
        $ordine->setIndirizzoConsegna($request->request->get('indirizzo'));
        $ordine->setCittaConsegna($request->request->get('citta'));
        $ordine->setDataEffettuazione(new\DateTime("now"));
        $ordine->setPrezzoTotale($request->request->get('totale'));
        $ordine->setConsegnato(false);
        $ordine->setRistorante($ristorante);
        $ordine->setTariffa($tariffa);
        $ordine->setCliente($this->get('security.token_storage')->getToken()->getUser()->getCliente());

        $pietanzeIds = $request->request->get('pietanze', []);
        $quantita = $request->request->get('quantita', []);

        foreach ($pietanzeIds as $key=>$id) {

          $pietanzaInOrdine = new \AppBundle\Entity\PietanzaInOrdine();

          $pietanza = $this->getDoctrine()
                           ->getRepository("AppBundle:Pietanza")
                           ->find($id);

          $pietanzaInOrdine->setQuantita($quantita[$key] ? $quantita[$key] : null);
          $pietanzaInOrdine->setPietanza($pietanza);
          $pietanzaInOrdine->setOrdine($ordine);

          $em->persist($pietanzaInOrdine);

        }

        $em->persist($ordine);
        $em->flush();

        $this->addFlash(
          "success",
          "Ordine effettuato con successo! N. Ordine: #" . $ordine->getCodOrdine() . "."
        );

        return $this->redirectToRoute("user_menu", array('id' => $ristorante->getId()));
      }

      return $this->render('view/menu/user_menu_dettagli.html.twig', array(
          'ristorante' => $ristorante
      ));

    }

    /**
     * @Route("/ristorante/menu/crea/{id}", name="admin_menu_crea")
     */
    public function newMenuAction($id, Request $request) {

      $ristorante = $this->getDoctrine()
                         ->getRepository("AppBundle:Ristorante")
                         ->find($id);

      if ($request->getMethod() === Request::METHOD_POST) {

        $em = $this->getDoctrine()->getManager();

        $menu = new \AppBundle\Entity\Menu();

        $menu->setDescrizione($request->request->get('descrizione'));
        $menu->setRistorante($ristorante);

        $sezioni = $request->request->get('sezioni', []);
        $descrizioni = $request->request->get('descrizioni', []);

        //file_put_contents("debug.txt", json_encode($sezioni), FILE_APPEND);

        for ($x = 0; $x < count($sezioni); $x++) {

          $sezione = new \AppBundle\Entity\Sezione();

          $sezione->setNome($sezioni[$x]);
          $sezione->setDescrizione($descrizioni[$x]);
          $sezione->setMenu($menu);

          $em->persist($sezione);
        }

        $em->persist($menu);
        $em->flush();

        $this->addFlash(
          "success",
          "Nuovo menu aggiunto!"
        );

        return $this->redirectToRoute("admin_menu", array('id' => $ristorante->getId()));
      }

      return $this->render('view/menu/menu_nuovo.html.twig', array(
          'ristorante' => $ristorante
      ));

    }

    /**
     * @Route("/ristorante/menu/edit/{id}", name="admin_menu_edit")
     */
    public function editMenuAction($id, Request $request) {

      $ristorante = $this->getDoctrine()
                         ->getRepository("AppBundle:Ristorante")
                         ->find($id);

      $menu = $this->getDoctrine()
                   ->getRepository("AppBundle:Menu")
                   ->find($ristorante->getMenu()->getId());

      if ($request->getMethod() === Request::METHOD_POST) {

        $sezioni = array();
        $descrizioni = array();

        $em = $this->getDoctrine()->getManager();

        $menu->setDescrizione($request->request->get('descrizione'));
        $menu->setRistorante($ristorante);

        $sezioni = $request->request->get('sezioni', []);
        $descrizioni = $request->request->get('descrizioni', []);

        file_put_contents("debug.txt", count($sezioni));

        for ($x = 0; $x < count($sezioni); $x++) {

          $sezione = new \AppBundle\Entity\Sezione();

          $sezione->setNome($sezioni[$x]);
          $sezione->setDescrizione($descrizioni[$x]);
          $sezione->setMenu($menu);

          $em->persist($sezione);
        }

        $em->persist($menu);
        $em->flush();

        $this->addFlash(
          "success",
          "Menù modificato!"
        );

        return $this->redirectToRoute("admin_menu", array('id' => $ristorante->getId()));
      }

      return $this->render('view/menu/menu_edit.html.twig', array(
          'ristorante' => $ristorante
      ));

    }


    /**
     * @Route("/ristorante/menu/delete/{id}", name="admin_menu_delete")
     */
    public function deleteMenuAction($id)  {

        $em = $this->getDoctrine()->getManager();
        $menu = $em->getRepository("AppBundle:Menu")->find($id);

        if (!$menu->getSezioni()->isEmpty()) {
          $sezioni = $menu->getSezioni();

          foreach ($sezioni as $sezione) {
            $em->remove($sezione);
          }
        }
        //sarà necessario eliminare anche i piatti

        $em->remove($menu);
        $em->flush();

        $this->addFlash(
          "success",
          "Menu rimosso correttamente!"
        );

        return $this->redirectToRoute("admin_ristorante_dettagli", array("id" => $menu->getRistorante()->getId()));
    }

    /**
     * @Route("/admin/ristorante/{id}/richiedi_fornitura", name="admin_richiesta_fornitura")
     */
    public function richiestaFornituraAction($id, Request $request)  {

      $ristorante = $this->getDoctrine()
                         ->getRepository("AppBundle:Ristorante")
                         ->find($id);

      $prodotti = $this->getDoctrine()
                       ->getRepository("AppBundle:Prodotto")
                       ->findAll();

      if ($request->getMethod() === Request::METHOD_POST) {

        $em = $this->getDoctrine()->getManager();

        $fornitura = new \AppBundle\Entity\Fornitura();

        $fornitoreId = $request->request->get('fornitore');
        $prodottoId = $request->request->get('prodotto');

        $prodotto = $this->getDoctrine()
                         ->getRepository("AppBundle:Prodotto")
                         ->find($prodottoId);

        $fornitore = $this->getDoctrine()
                          ->getRepository("AppBundle:Fornitore")
                          ->find($fornitoreId);

        $fornitura->setDataRichiesta(new\DateTime("now"));
        $fornitura->setRistorante($ristorante);
        $fornitura->setPrezzo($request->request->get('prezzo-totale'));
        $fornitura->setQuantita($request->request->get('quantita'));
        $fornitura->setFornitore($fornitore);
        $fornitura->setProdotto($prodotto);

        $em->persist($fornitura);
        $em->flush();

        $this->addFlash(
          "success",
          "Richiesta di fornitura effettuata!"
        );

        return $this->redirectToRoute("admin_ristorante_dettagli", array("id" => $ristorante->getId()));
      }

      return $this->render('view/forniture/fornitura_richiesta.html.twig', array(
        'ristorante' => $ristorante,
        'prodotti' => $prodotti
      ));

    }

    /**
     * @Route("/admin/ristorante/{id}/tavoli/", name="admin_ristorante_tavoli")
     */
    public function adminTavoliAction($id, Request $request) {

      $ristorante = $this->getDoctrine()
                         ->getRepository("AppBundle:Ristorante")
                         ->find($id);

      if ($request->getMethod() === Request::METHOD_POST) {

        $tavolo = new \AppBundle\Entity\Tavolo();

        $tavolo->setRistorante($ristorante);
        $tavolo->setNumero($request->request->get('numero'));
        $tavolo->setPosti($request->request->get('posti'));

        $coperti = ($ristorante->getMaxCoperti() == null) ? 0 : $ristorante->getMaxCoperti();

        $ristorante->setMaxCoperti($coperti + $tavolo->getPosti());


        $em = $this->getDoctrine()->getManager();
        $em->persist($tavolo);
        $em->persist($ristorante);
        $em->flush();

        $this->addFlash(
          "success",
          "Nuovo tavolo aggiunto!"
        );

        return $this->redirectToRoute("admin_ristorante_tavoli", array('id' => $id));
      }

      return $this->render('view/tavoli/tavoli_lista.html.twig', array(
        'ristorante' => $ristorante
      ));

    }


    /**
     * @Route("/admin/ristorante/{id}/magazzino/", name="admin_ristorante_magazzino")
     */
    public function adminMagazzinoAction($id, Request $request) {

      $ristorante = $this->getDoctrine()
                         ->getRepository("AppBundle:Ristorante")
                         ->find($id);

      if ($request->getMethod() === Request::METHOD_POST) {

        $em = $this->getDoctrine()->getManager();

        $p_id = $request->request->get('prodotto');
        $r_id = $request->request->get('ristorante');
        $quantita = $request->request->get('quantita');

        $prodottoInMagazzino = $em->getRepository("AppBundle:ProdottoInMagazzino")
                                  ->findOneBy(array(
                                    'ristorante' => $r_id,
                                    'prodotto' => $p_id
                                  ));

        $prodottoInMagazzino->setQuantita($quantita);

        $em->persist($prodottoInMagazzino);
        $em->flush();

        $this->addFlash(
          "success",
          "Prodotto modficato con successo!"
        );

        return $this->redirectToRoute("admin_ristorante_magazzino", array("id" => $ristorante->getId()));
     }


      return $this->render('view/magazzini/magazzino_dettagli.html.twig', array(
        'ristorante' => $ristorante
      ));

    }

    /**
     * @Route("/admin/ristorante/{id}/magazzino/i", name="admin_ristorante_magazzino_nuovo_prodotto")
     */
    public function newProductToListinoAction($id, Request $request) {

      $prodotti = $this->getDoctrine()
                       ->getRepository("AppBundle:Prodotto")
                       ->findAll();

      $ristorante = $this->getDoctrine()
                         ->getRepository("AppBundle:Ristorante")
                         ->find($id);

      if ($request->getMethod() === Request::METHOD_POST) {

        $em = $this->getDoctrine()->getManager();

        $productIds = $request->request->get('prodotti', []);
        $quantita = $request->request->get('quantita', []);

        foreach ($productIds as $key=>$id) {

          $prodottoInMagazzino = new \AppBundle\Entity\ProdottoInMagazzino();

          $prodotto = $this->getDoctrine()
                           ->getRepository("AppBundle:Prodotto")
                           ->find($id);

          $prodottoInMagazzino->setQuantita($quantita[$key] ? $quantita[$key] : null);
          $prodottoInMagazzino->setProdotto($prodotto);
          $prodottoInMagazzino->setRistorante($ristorante);

          $em->persist($prodottoInMagazzino);

        }

        $em->flush();

        $this->addFlash(
          "success",
          count($productIds) . " prodotti aggiunti al magazzino!"
        );

        return $this->redirectToRoute("admin_ristorante_magazzino", array("id" => $ristorante->getId()));
      }

      return $this->render('view/magazzini/magazzino_new_product.html.twig', array(
        'ristorante' => $ristorante,
        'prodotti' => $prodotti
      ));
    }

    /**
     * @Route("/admin/ristorante/{r_id}/elimina/{p_id}/", name="elimina_prodotto_magazzino")
     */
    public function deleteProductAction($r_id, $p_id)  {

        $em = $this->getDoctrine()->getManager();
        $prodottoInMagazzino = $em->getRepository("AppBundle:ProdottoInMagazzino")
                                  ->findOneBy(array(
                                    'ristorante' => $r_id,
                                    'prodotto' => $p_id
                                  ));

        $em->remove($prodottoInMagazzino);
        $em->flush();

        $this->addFlash(
          "success",
          "Prodotto rimosso dal magazzino!"
        );

        return $this->redirectToRoute("admin_ristorante_magazzino", array("id" => $r_id));
    }

    /**
     * @Route("/admin/ristorante/{id}/orari/", name="admin_ristorante_orari")
     */
    public function orariAction($id, Request $request)  {

      $ristorante = $this->getDoctrine()
                         ->getRepository("AppBundle:Ristorante")
                         ->find($id);

      if ($request->getMethod() === Request::METHOD_POST) {

        $em = $this->getDoctrine()->getManager();

        $giornoApertura = new \AppBundle\Entity\GiornoApertura();

        $giorno = $request->request->get('giorno');
        $aperturaPranzo = $request->request->get('apertura-pranzo');
        $chiusuraPranzo = $request->request->get('chiusura-pranzo');
        $aperturaCena = $request->request->get('apertura-cena');
        $chiusuraCena = $request->request->get('chiusura-cena');

        $giornoApertura->setGiorno($giorno);
        $giornoApertura->setRistorante($ristorante);

        if ($aperturaPranzo != null) {
          $giornoApertura->setOrarioAperturaPranzo(new\DateTime($aperturaPranzo));
          $giornoApertura->setOrarioChiusuraPranzo(new\DateTime($chiusuraPranzo));
        }
        if ($aperturaCena != null) {
          $giornoApertura->setOrarioAperturaCena(new\DateTime($aperturaCena));
          $giornoApertura->setOrarioChiusuraCena(new\DateTime($chiusuraCena));
        }


        $em->persist($giornoApertura);
        $em->flush();

        $this->addFlash(
          "success",
          "Giorno di apertura inserito con successo!"
        );
      }

      return $this->render('view/orari/orari_dettagli.html.twig', array(
        'ristorante' => $ristorante
      ));

    }

    /**
     * @Route("/admin/ristorante/{id}/check/orari/", name="admin_ristorante_check_orari")
     */
    public function checkGiornoAction($id, Request $request)  {

      $date = $request->request->get('date');

      $giornoApertura = $this->getDoctrine()
                             ->getRepository("AppBundle:GiornoApertura")
                             ->findOneBy(array(
                               'giorno' => $date,
                               'ristorante' => $id
                             ));

      $serializer = $this->container->get('jms_serializer');
      $giorno = $serializer->serialize(array("giornoApertura" => $giornoApertura), 'json');
      $response = new Response($giorno);
      $response->headers->set("Content-Type", "application/json");

      return $response;

    }

    /**
     * @Route("/user/ristorante/{id}/prenota/", name="user_ristorante_prenotazione")
     */
    public function prenotationAction($id, Request $request)  {

      $ristorante = $this->getDoctrine()
                         ->getRepository("AppBundle:Ristorante")
                         ->find($id);

      return $this->render('view/prenotazioni/prenotazione.html.twig', array(
        'ristorante' => $ristorante
      ));
    }

    /**
     * @Route("/user/ristorante/{id}/check/prenotazione/", name="user_ristorante_prenotazione_check")
     * @return boolean
     */
    public function checkAvailabilityAction($id, Request $request)  {

      $ristorante = $this->getDoctrine()
                         ->getRepository("AppBundle:Ristorante")
                         ->find($id);

      $date = $request->request->get('date');
      $orario = $request->request->get('orario');
      $persone = $request->request->get('persone');

      if (empty($date) || empty($orario) || empty($persone)) {
        $response = "La preghiamo di inserire tutti i parametri correttamente.";

        return new JsonResponse($response);
      }

      $giornoApertura = $this->getDoctrine()
                             ->getRepository("AppBundle:GiornoApertura")
                             ->findOneBy(array(
                               "ristorante" => $id,
                               "giorno" => $date
                             ));


      $data = \DateTime::createFromFormat('d/m/Y', $date);
      $startDate = \DateTime::createFromFormat('d/m/Y', $date);
      $endDate = \DateTime::createFromFormat('d/m/Y', $date);
      $time = explode(":", $orario);
      $data->setTime($time[0], $time[1]);
      $startDate->setTime($time[0] - 2, $time[1]);
      $endDate->setTime($time[0] + 2, $time[1]);

      if ($giornoApertura == null) {
        $response = "Ci dispiace ma il ristorante sarà chiuso nel giorno <strong>" . $date . "</strong>.";

        return new JsonResponse($response);
      } else {
        if ($orario < $giornoApertura->getOrarioAperturaPranzo()->format("H:i") ||
              ( $orario > $giornoApertura->getOrarioChiusuraPranzo()->format("H:i") &&
                $orario < $giornoApertura->getOrarioAperturaCena()->format("H:i") ) ||
            $orario > "22:30"
        ) {
            $response = "Ci dispiace ma l'orario inserito non corrisponde agli orari del ristorante.";

            return new JsonResponse($response);
        }
      }

      $startDate = $startDate->format('Y-m-d H:i:s');
      $endDate = $endDate->format('Y-m-d H:i:s');

      $sql = "SELECT numero, posti
              FROM tavolo t
              WHERE t.ristorante_id = $id
              AND (t.posti > ($persone - 1) AND t.posti < ($persone + 2))
              AND t.numero NOT IN (SELECT numeroTavolo
                                    FROM prenotazione p
                                    WHERE (p.data > '$startDate' AND p.data < '$endDate')
                                    AND p.codRistorante = $id)";

      $stmt = $this->getDoctrine()->getManager()->getConnection()->prepare($sql);
      $stmt->execute();
      $result = $stmt->fetchAll();

      if (count($result) == 0) {
        $response = "Il ristorante non ha disponibilità in questi orari. <br />
                   Se si desidera ne si possono provare altri.";
      } else {
        $serializer = $this->container->get('jms_serializer');
        $tavoli = $serializer->serialize(array("tavoli" => $result), 'json');
        $response = new Response($tavoli);
        $response->headers->set("Content-Type", "application/json");

        return $response;
      }

      return new JsonResponse($response);
    }

    /**
     * @Route("/user/ristorante/{id}/prenotazione/{numeroTavolo}/", name="user_ristorante_conferma_prenotazione")
     */
    public function prenotazioneAction($id, $numeroTavolo, Request $request)  {

      $ristorante = $this->getDoctrine()
                         ->getRepository("AppBundle:Ristorante")
                         ->find($id);

      $tavolo = $this->getDoctrine()
                     ->getRepository("AppBundle:Tavolo")
                     ->findOneBy(array(
                        "ristorante" => $id,
                        "numero" => $numeroTavolo
                      ));

      $date = $request->request->get('date');
      $orario = $request->request->get('orario');
      $persone = $request->request->get('persone');

      $orarioTmp = explode(":", $orario);

      $data = \DateTime::createFromFormat('d/m/Y', $date);
      $data->setTime($orarioTmp[0], $orarioTmp[1]);
      $orario = \DateTime::createFromFormat('H:i', $orario);

      $em = $this->getDoctrine()->getManager();

      $prenotazione = new \AppBundle\Entity\Prenotazione();

      $prenotazione->setTavolo($tavolo);
      $prenotazione->setPersone($persone);
      $prenotazione->setData($data);
      $prenotazione->setOrario($orario);
      $prenotazione->setCliente($this->get('security.token_storage')->getToken()->getUser()->getCliente());

      $em->persist($prenotazione);
      $em->flush();

      $response = "Confermiamo la sua prenotazione in data <strong>" . $date . "</strong> alle ore <strong>"
                  . $orario->format("H:i") . "</strong> per <strong>" . $persone . " persone</strong>.<br/>
                  Le abbiamo assegnato il tavolo numero <strong>" . $numeroTavolo . ".";

      return new JsonResponse($response);
    }

    /**
     * @Route("/admin/ristorante/{id}/tariffario/", name="admin_ristorante_tariffario")
     */
    public function tariffarioAction($id, Request $request)  {

      $ristorante = $this->getDoctrine()
                         ->getRepository("AppBundle:Ristorante")
                         ->find($id);

      if ($request->getMethod() === Request::METHOD_POST) {

        $em = $this->getDoctrine()->getManager();

        $tariffario = new \AppBundle\Entity\Tariffario();

        $tariffario->setCap($request->request->get('cap'));
        $tariffario->setPrezzo($request->request->get('tariffa'));
        $tariffario->setRistorante($ristorante);

        $em->persist($tariffario);
        $em->flush();

        $this->addFlash(
          "success",
          "Nuova tariffa inserita!"
        );

        return $this->redirectToRoute("admin_ristorante_tariffario", array('id' => $id));
      }

      return $this->render('view/tariffari/tariffario_dettagli.html.twig', array(
        'ristorante' => $ristorante
      ));
    }

    /**
     * @Route("/user/ristorante/{id}/check/orari/", name="user_ristorante_check_orari")
     */
    public function checkOrariAction($id, Request $request)  {

      $data = $request->request->get('date');

      $giornoApertura = $this->getDoctrine()
                             ->getRepository("AppBundle:GiornoApertura")
                             ->findOneBy(array(
                               'giorno' => $data,
                               'ristorante' => $id
                             ));

      $ora = $request->request->get('orario');

      if (($ora > $giornoApertura->getOrarioAperturaPranzo()->format("H:i") &&
           $ora < $giornoApertura->getOrarioChiusuraPranzo()->format("H:i")) ||
          ($ora > $giornoApertura->getOrarioAperturaCena()->format("H:i") &&
           $ora < $giornoApertura->getOrarioChiusuraCena()->format("H:i"))) {

        return new JsonResponse(true);
      } else {
        return new JsonResponse(false);
      }

    }

    /**
     * @Route("/admin/ristorante/{id}/personale/", name="admin_ristorante_personale")
     */
    public function personaleAction($id, Request $request)  {

      $ristorante = $this->getDoctrine()
                         ->getRepository("AppBundle:Ristorante")
                         ->find($id);

      if ($request->getMethod() === Request::METHOD_POST) {

        $em = $this->getDoctrine()->getManager();

        if ($request->request->get('turno') == "true") {

          $data = \DateTime::createFromFormat('Y-m-d', $request->request->get('giorno'))->format("d/m/Y");

          $giornoApertura = $this->getDoctrine()
                                 ->getRepository("AppBundle:GiornoApertura")
                                 ->find(array(
                                    'giorno' => $data,
                                    'ristorante' => $id
                                  ));

          $dipendente = $this->getDoctrine()
                         ->getRepository("AppBundle:Dipendente")
                         ->find($request->request->get('dipendente'));

          $turno = new \AppBundle\Entity\Turno();

          $turno->setDipendente($dipendente);
          $turno->setGiornoApertura($giornoApertura);
          $turno->setFascia($request->request->get('fascia'));
          $turno->setOraInizio(new\DateTime($request->request->get('inizio-turno')));
          $turno->setOraFine(new\DateTime($request->request->get('fine-turno')));

          $em->persist($turno);
          $em->flush();

          $this->addFlash(
            "success",
            "Nuovo turno inserito!"
          );

        } else {

          $dipendente = new \AppBundle\Entity\Dipendente();

          $dipendente->setNome($request->request->get('nome'));
          $dipendente->setCognome($request->request->get('cognome'));
          $dipendente->setCodiceFiscale($request->request->get('codice-fiscale'));
          $dipendente->setIndirizzo($request->request->get('indirizzo'));
          $dipendente->setCitta($request->request->get('citta'));
          $dipendente->setTelefono($request->request->get('telefono'));
          $dipendente->setRuolo($request->request->get('ruolo'));
          $dipendente->setDataAssunzione(new\DateTime($request->request->get('assunzione')));

          if ($request->request->get('fine-contratto') != null) {
            $dipendente->setDataFineContratto(new\DateTime($request->request->get('fine-contratto')));
          }

          $dipendente->setStipendio($request->request->get('stipendio'));

          $dipendente->setRistorante($ristorante);

          $em->persist($dipendente);
          $em->flush();

          $this->addFlash(
            "success",
            "Nuovo dipendente inserito!"
          );

        }

        return $this->redirectToRoute("admin_ristorante_personale", array('id' => $id));
      }

      return $this->render('view/personale/personale_dettagli.html.twig', array(
        'ristorante' => $ristorante
      ));
    }

    /**
     * @Route("/admin/ristorante/{id}/check/turni/", name="admin_ristorante_check_turni")
     */
    public function checkTurniAction($id, Request $request)  {

      $data = $request->request->get('date');

      $giornoApertura = $this->getDoctrine()
                             ->getRepository("AppBundle:GiornoApertura")
                             ->findOneBy(array(
                               'giorno' => $data,
                               'ristorante' => $id
                             ));

      $ora = $request->request->get('orario');

      if ($giornoApertura == null) {

        return new JsonResponse(false);

      } else {

        $turniTmp = $giornoApertura->getTurni();

        $serializer = $this->container->get('jms_serializer');
        $turni = $serializer->serialize(array("turni" => $turniTmp), 'json');
        $response = new Response($turni);
        $response->headers->set("Content-Type", "application/json");

        return $response;
      }

    }

    /**
     * @Route("/user/ristoranti/ordini/", name="user_ristoranti_ordini")
     */
    public function ordiniAction(Request $request)  {

      $cliente = $this->get('security.token_storage')->getToken()->getUser()->getCliente();

      if ($request->getMethod() === Request::METHOD_POST) {

        $em = $this->getDoctrine()->getManager();

        $recensione = new \AppBundle\Entity\Recensione();

        $ordine = $this->getDoctrine()
                       ->getRepository("AppBundle:Ordine")
                       ->find($request->request->get('ordine'));

        $ristorante = $this->getDoctrine()
                       ->getRepository("AppBundle:Ristorante")
                       ->find($request->request->get('ristorante'));

        $recensione->setData(new\DateTime("now"));
        $recensione->setTitolo($request->request->get('titolo'));
        $recensione->setTesto($request->request->get('testo'));
        $recensione->setVoto($request->request->get('voto'));
        $recensione->setRistorante($ristorante);
        $recensione->setOrdine($ordine);

        $ordine->setRecensione($recensione);

        $em->persist($ordine);
        $em->persist($recensione);
        $em->flush();

        $this->addFlash(
          "success",
          "Nuova recensione inserita!"
        );

        return $this->redirectToRoute("user_ristoranti_ordini");
      }

      return $this->render('view/ordini/ordini_dettagli.html.twig', array(
        'cliente' => $cliente
      ));
    }


    /**
     * @Route("/user/ristoranti/prenotazioni/", name="user_ristoranti_prenotazioni")
     */
    public function prenotazioniAction()  {

      $cliente = $this->get('security.token_storage')->getToken()->getUser()->getCliente();

      return $this->render('view/prenotazioni/prenotazioni_dettagli.html.twig', array(
        'cliente' => $cliente
      ));
    }

    /**
     * @Route("/admin/ristorante/{r_id}conferma_ordine/{o_id}/", name="admin_ristorante_ordine_conferma")
     */
    public function confermaOrdineAction($r_id, $o_id)  {

      $ordine = $this->getDoctrine()
                     ->getRepository("AppBundle:Ordine")
                     ->find($o_id);

      $em = $this->getDoctrine()->getManager();

      $ordine->setConsegnato(true);

      $em->persist($ordine);
      $em->flush();

      return $this->redirectToRoute("admin_ristorante_dettagli", array('id' => $r_id));
    }


    /**
     * @Route("/admin/ristorante/{id}/check/turno/", name="admin_ristorante_check_turno")
     */
    public function checkTurnoAction($id, Request $request)  {

      $data = \DateTime::createFromFormat('Y-m-d', $request->request->get('date'))->format("d/m/Y");
      $fascia = $request->request->get('fascia');
      $codDipendente = $request->request->get('codDipendente');

      $sql = "SELECT *
              FROM turno t
              WHERE t.giorno = '$data'
              AND t.fascia = '$fascia'
              AND t.codRistorante = $id
              AND t.codDipendente = $codDipendente";

      $stmt = $this->getDoctrine()->getManager()->getConnection()->prepare($sql);
      $stmt->execute();
      $turno = $stmt->fetchAll();

      if ($turno == null) {
        return new JsonResponse(false);
      } else {
        return new JsonResponse(true);;
      }

    }


}
