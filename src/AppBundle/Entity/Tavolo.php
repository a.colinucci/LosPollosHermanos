<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tavolo
 *
 * @ORM\Table(name="tavolo")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TavoloRepository")
 */
class Tavolo
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(name="numero", type="integer")
     */
    private $numero;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Ristorante", inversedBy="tavoli")
     */
    private $ristorante;

    /**
     * @var int
     *
     * @ORM\Column(name="posti", type="integer")
     */
    private $posti;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Prenotazione", mappedBy="tavolo")
     **/
    private $prenotazioni;

    /**
     * Set numero
     *
     * @param integer $numero
     *
     * @return Tavolo
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return integer
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set posti
     *
     * @param integer $posti
     *
     * @return Tavolo
     */
    public function setPosti($posti)
    {
        $this->posti = $posti;

        return $this;
    }

    /**
     * Get posti
     *
     * @return integer
     */
    public function getPosti()
    {
        return $this->posti;
    }

    /**
     * Set ristorante
     *
     * @param \AppBundle\Entity\Ristorante $ristorante
     *
     * @return Tavolo
     */
    public function setRistorante(\AppBundle\Entity\Ristorante $ristorante)
    {
        $this->ristorante = $ristorante;

        return $this;
    }

    /**
     * Get ristorante
     *
     * @return \AppBundle\Entity\Ristorante
     */
    public function getRistorante()
    {
        return $this->ristorante;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->prenotazioni = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add prenotazioni
     *
     * @param \AppBundle\Entity\Prenotazione $prenotazioni
     *
     * @return Tavolo
     */
    public function addPrenotazioni(\AppBundle\Entity\Prenotazione $prenotazioni)
    {
        $this->prenotazioni[] = $prenotazioni;

        return $this;
    }

    /**
     * Remove prenotazioni
     *
     * @param \AppBundle\Entity\Prenotazione $prenotazioni
     */
    public function removePrenotazioni(\AppBundle\Entity\Prenotazione $prenotazioni)
    {
        $this->prenotazioni->removeElement($prenotazioni);
    }

    /**
     * Get prenotazioni
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPrenotazioni()
    {
        return $this->prenotazioni;
    }
}
