<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Prenotazione
 *
 * @ORM\Table(name="prenotazione")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PrenotazioneRepository")
 */
class Prenotazione
{
    /**
     * @var int
     *
     * @ORM\Column(name="codPrenotazione", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $codPrenotazione;

    /**
     * @var int
     *
     * @ORM\Column(name="persone", type="integer")
     */
    private $persone;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data", type="datetime")
     */
    private $data;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="orario", type="time")
     */
    private $orario;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Tavolo", inversedBy="prenotazioni")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="numeroTavolo", referencedColumnName="numero", nullable=false),
     *   @ORM\JoinColumn(name="codRistorante", referencedColumnName="ristorante_id", nullable=false)
     * })
     */
    private $tavolo;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Cliente", inversedBy="prenotazioni")
     * @ORM\JoinColumn(name="codCliente", referencedColumnName="id")
     */
    private $cliente;


    /**
     * Get codPrenotazione
     *
     * @return int
     */
    public function getCodPrenotazione()
    {
        return $this->codPrenotazione;
    }

    /**
     * Set persone
     *
     * @param integer $persone
     *
     * @return Prenotazione
     */
    public function setPersone($persone)
    {
        $this->persone = $persone;

        return $this;
    }

    /**
     * Get persone
     *
     * @return int
     */
    public function getPersone()
    {
        return $this->persone;
    }

    /**
     * Set data
     *
     * @param \DateTime $data
     *
     * @return Prenotazione
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return \DateTime
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set orario
     *
     * @param \DateTime $orario
     *
     * @return Prenotazione
     */
    public function setOrario($orario)
    {
        $this->orario = $orario;

        return $this;
    }

    /**
     * Get orario
     *
     * @return \DateTime
     */
    public function getOrario()
    {
        return $this->orario;
    }

    /**
     * Set tavolo
     *
     * @param \AppBundle\Entity\Tavolo $tavolo
     *
     * @return Prenotazione
     */
    public function setTavolo(\AppBundle\Entity\Tavolo $tavolo)
    {
        $this->tavolo = $tavolo;

        return $this;
    }

    /**
     * Get tavolo
     *
     * @return \AppBundle\Entity\Tavolo
     */
    public function getTavolo()
    {
        return $this->tavolo;
    }

    /**
     * Set cliente
     *
     * @param \AppBundle\Entity\Cliente $cliente
     *
     * @return Prenotazione
     */
    public function setCliente(\AppBundle\Entity\Cliente $cliente = null)
    {
        $this->cliente = $cliente;

        return $this;
    }

    /**
     * Get cliente
     *
     * @return \AppBundle\Entity\Cliente
     */
    public function getCliente()
    {
        return $this->cliente;
    }
}
