<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Ordine
 *
 * @ORM\Table(name="ordine")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OrdineRepository")
 */
class Ordine
{
    /**
     * @var int
     *
     * @ORM\Column(name="codOrdine", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $codOrdine;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dataEffettuazione", type="datetime")
     */
    private $dataEffettuazione;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="oraConsegna", type="time")
     */
    private $oraConsegna;

    /**
     * @var string
     *
     * @ORM\Column(name="cittaConsegna", type="string", length=100)
     */
    private $cittaConsegna;

    /**
     * @var string
     *
     * @ORM\Column(name="indirizzoConsegna", type="string", length=255)
     */
    private $indirizzoConsegna;

    /**
     * @var float
     *
     * @ORM\Column(name="prezzoTotale", type="float")
     */
    private $prezzoTotale;

    /**
     * @var bool
     *
     * @ORM\Column(name="consegnato", type="boolean")
     */
    private $consegnato;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Ristorante", inversedBy="ordini")
     * @ORM\JoinColumn(name="codRistorante", referencedColumnName="id")
     */
    private $ristorante;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Tariffario", inversedBy="ordini")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cap", referencedColumnName="cap", nullable=false),
     *   @ORM\JoinColumn(name="codRistorante", referencedColumnName="codRistorante", nullable=false)
     * })
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $tariffa;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Cliente", inversedBy="ordini")
     */
    private $cliente;

    /**
     * @ORM\OneToMany(targetEntity="PietanzaInOrdine", mappedBy="ordine")
     */
    protected $pietanzeInOrdine;

    /**
     * @var ArrayCollection
     * @ORM\OneToOne(targetEntity="Recensione")
     * @ORM\JoinColumn(name="codRecensione", referencedColumnName="codRecensione")
     **/
    private $recensione;

    /**
     * Get codOrdine
     *
     * @return int
     */
    public function getCodOrdine()
    {
        return $this->codOrdine;
    }

    /**
     * Set dataEffettuazione
     *
     * @param \DateTime $dataEffettuazione
     *
     * @return Ordine
     */
    public function setDataEffettuazione($dataEffettuazione)
    {
        $this->dataEffettuazione = $dataEffettuazione;

        return $this;
    }

    /**
     * Get dataEffettuazione
     *
     * @return \DateTime
     */
    public function getDataEffettuazione()
    {
        return $this->dataEffettuazione;
    }

    /**
     * Set oraConsegna
     *
     * @param \DateTime $oraConsegna
     *
     * @return Ordine
     */
    public function setOraConsegna($oraConsegna)
    {
        $this->oraConsegna = $oraConsegna;

        return $this;
    }

    /**
     * Get oraConsegna
     *
     * @return \DateTime
     */
    public function getOraConsegna()
    {
        return $this->oraConsegna;
    }

    /**
     * Set cittaConsegna
     *
     * @param string $cittaConsegna
     *
     * @return Ordine
     */
    public function setCittaConsegna($cittaConsegna)
    {
        $this->cittaConsegna = $cittaConsegna;

        return $this;
    }

    /**
     * Get cittaConsegna
     *
     * @return string
     */
    public function getCittaConsegna()
    {
        return $this->cittaConsegna;
    }

    /**
     * Set indirizzoConsegna
     *
     * @param string $indirizzoConsegna
     *
     * @return Ordine
     */
    public function setIndirizzoConsegna($indirizzoConsegna)
    {
        $this->indirizzoConsegna = $indirizzoConsegna;

        return $this;
    }

    /**
     * Get indirizzoConsegna
     *
     * @return string
     */
    public function getIndirizzoConsegna()
    {
        return $this->indirizzoConsegna;
    }

    /**
     * Set prezzoTotale
     *
     * @param float $prezzoTotale
     *
     * @return Ordine
     */
    public function setPrezzoTotale($prezzoTotale)
    {
        $this->prezzoTotale = $prezzoTotale;

        return $this;
    }

    /**
     * Get prezzoTotale
     *
     * @return float
     */
    public function getPrezzoTotale()
    {
        return $this->prezzoTotale;
    }

    /**
     * Set consegnato
     *
     * @param boolean $consegnato
     *
     * @return Ordine
     */
    public function setConsegnato($consegnato)
    {
        $this->consegnato = $consegnato;

        return $this;
    }

    /**
     * Get consegnato
     *
     * @return bool
     */
    public function getConsegnato()
    {
        return $this->consegnato;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->pietanzeInOrdine = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set ristorante
     *
     * @param \AppBundle\Entity\Ristorante $ristorante
     *
     * @return Ordine
     */
    public function setRistorante(\AppBundle\Entity\Ristorante $ristorante = null)
    {
        $this->ristorante = $ristorante;

        return $this;
    }

    /**
     * Get ristorante
     *
     * @return \AppBundle\Entity\Ristorante
     */
    public function getRistorante()
    {
        return $this->ristorante;
    }

    /**
     * Set tariffa
     *
     * @param \AppBundle\Entity\Tariffario $tariffa
     *
     * @return Ordine
     */
    public function setTariffa(\AppBundle\Entity\Tariffario $tariffa)
    {
        $this->tariffa = $tariffa;

        return $this;
    }

    /**
     * Get tariffa
     *
     * @return \AppBundle\Entity\Tariffario
     */
    public function getTariffa()
    {
        return $this->tariffa;
    }

    /**
     * Set cliente
     *
     * @param \AppBundle\Entity\Cliente $cliente
     *
     * @return Ordine
     */
    public function setCliente(\AppBundle\Entity\Cliente $cliente = null)
    {
        $this->cliente = $cliente;

        return $this;
    }

    /**
     * Get cliente
     *
     * @return \AppBundle\Entity\Cliente
     */
    public function getCliente()
    {
        return $this->cliente;
    }

    /**
     * Add pietanzeInOrdine
     *
     * @param \AppBundle\Entity\PietanzaInOrdine $pietanzeInOrdine
     *
     * @return Ordine
     */
    public function addPietanzeInOrdine(\AppBundle\Entity\PietanzaInOrdine $pietanzeInOrdine)
    {
        $this->pietanzeInOrdine[] = $pietanzeInOrdine;

        return $this;
    }

    /**
     * Remove pietanzeInOrdine
     *
     * @param \AppBundle\Entity\PietanzaInOrdine $pietanzeInOrdine
     */
    public function removePietanzeInOrdine(\AppBundle\Entity\PietanzaInOrdine $pietanzeInOrdine)
    {
        $this->pietanzeInOrdine->removeElement($pietanzeInOrdine);
    }

    /**
     * Get pietanzeInOrdine
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPietanzeInOrdine()
    {
        return $this->pietanzeInOrdine;
    }

    /**
     * Set recensione
     *
     * @param \AppBundle\Entity\Recensione $recensione
     *
     * @return Ordine
     */
    public function setRecensione(\AppBundle\Entity\Recensione $recensione = null)
    {
        $this->recensione = $recensione;

        return $this;
    }

    /**
     * Get recensione
     *
     * @return \AppBundle\Entity\Recensione
     */
    public function getRecensione()
    {
        return $this->recensione;
    }
}
