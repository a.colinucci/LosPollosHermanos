<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Sezione
 *
 * @ORM\Table(name="sezione")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SezioneRepository")
 */
class Sezione
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=30)
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(name="descrizione", type="string", length=500, nullable=true)
     */
    private $descrizione;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Menu", inversedBy="sezioni")
     */
    private $menu;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Pietanza", mappedBy="sezione")
     **/
    private $pietanze;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nome
     *
     * @param string $nome
     *
     * @return Sezione
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set descrizione
     *
     * @param string $descrizione
     *
     * @return Sezione
     */
    public function setDescrizione($descrizione)
    {
        $this->descrizione = $descrizione;

        return $this;
    }

    /**
     * Get descrizione
     *
     * @return string
     */
    public function getDescrizione()
    {
        return $this->descrizione;
    }

    /**
     * Set menu
     *
     * @param \AppBundle\Entity\Menu $menu
     *
     * @return Sezione
     */
    public function setMenu(\AppBundle\Entity\Menu $menu = null)
    {
        $this->menu = $menu;

        return $this;
    }

    /**
     * Get menu
     *
     * @return \AppBundle\Entity\Menu
     */
    public function getMenu()
    {
        return $this->menu;
    }

    public function __toString() {
        return $this->nome;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->pietanze = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add pietanze
     *
     * @param \AppBundle\Entity\Pietanza $pietanze
     *
     * @return Sezione
     */
    public function addPietanze(\AppBundle\Entity\Pietanza $pietanze)
    {
        $this->pietanze[] = $pietanze;

        return $this;
    }

    /**
     * Remove pietanze
     *
     * @param \AppBundle\Entity\Pietanza $pietanze
     */
    public function removePietanze(\AppBundle\Entity\Pietanza $pietanze)
    {
        $this->pietanze->removeElement($pietanze);
    }

    /**
     * Get pietanze
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPietanze()
    {
        return $this->pietanze;
    }
}
