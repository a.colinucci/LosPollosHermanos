<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Ristorante
 *
 * @ORM\Table(name="ristorante")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RistoranteRepository")
 */
class Ristorante
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=50)
     */
    private $nome;

    /**
     * @var int
     *
     * @ORM\Column(name="maxCoperti", type="integer", nullable=true)
     */
    private $maxCoperti;

    /**
     * @var string
     *
     * @ORM\Column(name="indirizzo", type="string", length=100)
     */
    private $indirizzo;

    /**
     * @var string
     *
     * @ORM\Column(name="citta", type="string", length=100)
     */
    private $citta;

    /**
     * @var string
     *
     * @ORM\Column(name="telefono", type="string", length=30)
     */
    private $telefono;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Tavolo", mappedBy="ristorante")
     **/
    private $tavoli;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Recensione", mappedBy="ristorante")
     **/
    private $recensioni;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Dipendente", mappedBy="ristorante")
     **/
    private $dipendenti;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="GiornoApertura", mappedBy="ristorante")
     **/
    private $giorniApertura;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Fornitura", mappedBy="ristorante")
     **/
    private $forniture;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Tariffario", mappedBy="ristorante")
     **/
    private $tariffari;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Ordine", mappedBy="ristorante")
     **/
    private $ordini;

    /**
     * @var ArrayCollection
     * @ORM\OneToOne(targetEntity="Menu", mappedBy="ristorante")
     **/
    private $menu;

    /**
     * @ORM\ManyToMany(targetEntity="Fornitore", mappedBy="ristoranti")
     */
    private $fornitori;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="ProdottoInMagazzino", mappedBy="ristorante")
     **/
    private $prodottiInMagazzino;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nome
     *
     * @param string $nome
     *
     * @return Ristorante
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set maxCoperti
     *
     * @param integer $maxCoperti
     *
     * @return Ristorante
     */
    public function setMaxCoperti($maxCoperti)
    {
        $this->maxCoperti = $maxCoperti;

        return $this;
    }

    /**
     * Get maxCoperti
     *
     * @return integer
     */
    public function getMaxCoperti()
    {
        return $this->maxCoperti;
    }

    /**
     * Set indirizzo
     *
     * @param string $indirizzo
     *
     * @return Ristorante
     */
    public function setIndirizzo($indirizzo)
    {
        $this->indirizzo = $indirizzo;

        return $this;
    }

    /**
     * Get indirizzo
     *
     * @return string
     */
    public function getIndirizzo()
    {
        return $this->indirizzo;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->tavoli = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add tavoli
     *
     * @param \AppBundle\Entity\Tavolo $tavoli
     *
     * @return Ristorante
     */
    public function addTavoli(\AppBundle\Entity\Tavolo $tavoli)
    {
        $this->tavoli[] = $tavoli;

        return $this;
    }

    /**
     * Remove tavoli
     *
     * @param \AppBundle\Entity\Tavolo $tavoli
     */
    public function removeTavoli(\AppBundle\Entity\Tavolo $tavoli)
    {
        $this->tavoli->removeElement($tavoli);
    }

    /**
     * Get tavoli
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTavoli()
    {
        return $this->tavoli;
    }

    /**
     * Add forniture
     *
     * @param \AppBundle\Entity\Fornitura $forniture
     *
     * @return Ristorante
     */
    public function addForniture(\AppBundle\Entity\Fornitura $forniture)
    {
        $this->forniture[] = $forniture;

        return $this;
    }

    /**
     * Remove forniture
     *
     * @param \AppBundle\Entity\Fornitura $forniture
     */
    public function removeForniture(\AppBundle\Entity\Fornitura $forniture)
    {
        $this->forniture->removeElement($forniture);
    }

    /**
     * Get forniture
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getForniture()
    {
        return $this->forniture;
    }

    /**
     * Add menu
     *
     * @param \AppBundle\Entity\Menu $menu
     *
     * @return Ristorante
     */
    public function addMenu(\AppBundle\Entity\Menu $menu)
    {
        $this->menu[] = $menu;

        return $this;
    }

    /**
     * Remove menu
     *
     * @param \AppBundle\Entity\Menu $menu
     */
    public function removeMenu(\AppBundle\Entity\Menu $menu)
    {
        $this->menu->removeElement($menu);
    }

    /**
     * Get menu
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMenu()
    {
        return $this->menu;
    }

    /**
     * Add tariffari
     *
     * @param \AppBundle\Entity\Tariffario $tariffari
     *
     * @return Ristorante
     */
    public function addTariffari(\AppBundle\Entity\Tariffario $tariffari)
    {
        $this->tariffari[] = $tariffari;

        return $this;
    }

    /**
     * Remove tariffari
     *
     * @param \AppBundle\Entity\Tariffario $tariffari
     */
    public function removeTariffari(\AppBundle\Entity\Tariffario $tariffari)
    {
        $this->tariffari->removeElement($tariffari);
    }

    /**
     * Get tariffari
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTariffari()
    {
        return $this->tariffari;
    }

    /**
     * Set menu
     *
     * @param \AppBundle\Entity\Menu $menu
     *
     * @return Ristorante
     */
    public function setMenu(\AppBundle\Entity\Menu $menu = null)
    {
        $this->menu = $menu;

        return $this;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     *
     * @return Ristorante
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Add fornitori
     *
     * @param \AppBundle\Entity\Fornitore $fornitori
     *
     * @return Ristorante
     */
    public function addFornitori(\AppBundle\Entity\Fornitore $fornitori)
    {
        $this->fornitori[] = $fornitori;

        return $this;
    }

    /**
     * Remove fornitori
     *
     * @param \AppBundle\Entity\Fornitore $fornitori
     */
    public function removeFornitori(\AppBundle\Entity\Fornitore $fornitori)
    {
        $this->fornitori->removeElement($fornitori);
    }

    /**
     * Get fornitori
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFornitori()
    {
        return $this->fornitori;
    }

    /**
     * Add prodottiInMagazzino
     *
     * @param \AppBundle\Entity\ProdottoInMagazzino $prodottiInMagazzino
     *
     * @return Ristorante
     */
    public function addProdottiInMagazzino(\AppBundle\Entity\ProdottoInMagazzino $prodottiInMagazzino)
    {
        $this->prodottiInMagazzino[] = $prodottiInMagazzino;

        return $this;
    }

    /**
     * Remove prodottiInMagazzino
     *
     * @param \AppBundle\Entity\ProdottoInMagazzino $prodottiInMagazzino
     */
    public function removeProdottiInMagazzino(\AppBundle\Entity\ProdottoInMagazzino $prodottiInMagazzino)
    {
        $this->prodottiInMagazzino->removeElement($prodottiInMagazzino);
    }

    /**
     * Get prodottiInMagazzino
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProdottiInMagazzino()
    {
        return $this->prodottiInMagazzino;
    }

    /**
     * Add giorniApertura
     *
     * @param \AppBundle\Entity\GiornoApertura $giorniApertura
     *
     * @return Ristorante
     */
    public function addGiorniApertura(\AppBundle\Entity\GiornoApertura $giorniApertura)
    {
        $this->giorniApertura[] = $giorniApertura;

        return $this;
    }

    /**
     * Remove giorniApertura
     *
     * @param \AppBundle\Entity\GiornoApertura $giorniApertura
     */
    public function removeGiorniApertura(\AppBundle\Entity\GiornoApertura $giorniApertura)
    {
        $this->giorniApertura->removeElement($giorniApertura);
    }

    /**
     * Get giorniApertura
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGiorniApertura()
    {
        return $this->giorniApertura;
    }

    /**
     * Set citta
     *
     * @param string $citta
     *
     * @return Ristorante
     */
    public function setCitta($citta)
    {
        $this->citta = $citta;

        return $this;
    }

    /**
     * Get citta
     *
     * @return string
     */
    public function getCitta()
    {
        return $this->citta;
    }


    /**
     * Add ordini
     *
     * @param \AppBundle\Entity\Ordine $ordini
     *
     * @return Ristorante
     */
    public function addOrdini(\AppBundle\Entity\Ordine $ordini)
    {
        $this->ordini[] = $ordini;

        return $this;
    }

    /**
     * Remove ordini
     *
     * @param \AppBundle\Entity\Ordine $ordini
     */
    public function removeOrdini(\AppBundle\Entity\Ordine $ordini)
    {
        $this->ordini->removeElement($ordini);
    }

    /**
     * Get ordini
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrdini()
    {
        return $this->ordini;
    }

    /**
     * Add dipendenti
     *
     * @param \AppBundle\Entity\Dipendente $dipendenti
     *
     * @return Ristorante
     */
    public function addDipendenti(\AppBundle\Entity\Dipendente $dipendenti)
    {
        $this->dipendenti[] = $dipendenti;

        return $this;
    }

    /**
     * Remove dipendenti
     *
     * @param \AppBundle\Entity\Dipendente $dipendenti
     */
    public function removeDipendenti(\AppBundle\Entity\Dipendente $dipendenti)
    {
        $this->dipendenti->removeElement($dipendenti);
    }

    /**
     * Get dipendenti
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDipendenti()
    {
        return $this->dipendenti;
    }

    /**
     * Add recensioni
     *
     * @param \AppBundle\Entity\Recensione $recensioni
     *
     * @return Ristorante
     */
    public function addRecensioni(\AppBundle\Entity\Recensione $recensioni)
    {
        $this->recensioni[] = $recensioni;

        return $this;
    }

    /**
     * Remove recensioni
     *
     * @param \AppBundle\Entity\Recensione $recensioni
     */
    public function removeRecensioni(\AppBundle\Entity\Recensione $recensioni)
    {
        $this->recensioni->removeElement($recensioni);
    }

    /**
     * Get recensioni
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRecensioni()
    {
        return $this->recensioni;
    }
}
