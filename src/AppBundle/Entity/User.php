<?php
// src/AppBundle/Entity/User.php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * One fornitore has One Account.
     * @ORM\OneToOne(targetEntity="Fornitore", inversedBy="user")
     * @ORM\JoinColumn(name="fornitore_id", referencedColumnName="id")
     */
    private $fornitore;

    /**
     * One cliente has One Account.
     * @ORM\OneToOne(targetEntity="Cliente", inversedBy="user")
     * @ORM\JoinColumn(name="cliente_id", referencedColumnName="id")
     */
    private $cliente;

    public function __construct()
    {
        parent::__construct();
        // your own logic
    }


    /**
     * Set fornitore
     *
     * @param \AppBundle\Entity\Fornitore $fornitore
     *
     * @return User
     */
    public function setFornitore(\AppBundle\Entity\Fornitore $fornitore = null)
    {
        $this->fornitore = $fornitore;

        return $this;
    }

    /**
     * Get fornitore
     *
     * @return \AppBundle\Entity\Fornitore
     */
    public function getFornitore()
    {
        return $this->fornitore;
    }

    /**
     * Set cliente
     *
     * @param \AppBundle\Entity\Cliente $cliente
     *
     * @return User
     */
    public function setCliente(\AppBundle\Entity\Cliente $cliente = null)
    {
        $this->cliente = $cliente;

        return $this;
    }

    /**
     * Get cliente
     *
     * @return \AppBundle\Entity\Cliente
     */
    public function getCliente()
    {
        return $this->cliente;
    }
}
