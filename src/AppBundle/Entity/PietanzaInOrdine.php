<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PietanzaInOrdine
 *
 * @ORM\Table(name="pietanza_in_ordine")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PietanzaInOrdineRepository")
 */
class PietanzaInOrdine
{
    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Ordine", inversedBy="pietanzeInOrdine")
     * @ORM\JoinColumn(name="codOrdine", referencedColumnName="codOrdine", nullable=FALSE)
     */
    protected $ordine;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Pietanza", inversedBy="pietanzaInOrdini")
     * @ORM\JoinColumn(name="codPietanza", referencedColumnName="codPietanza", nullable=FALSE)
     */
    protected $pietanza;

    /**
     * @var int
     *
     * @ORM\Column(name="quantita", type="integer")
     */
    private $quantita;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set quantita
     *
     * @param integer $quantita
     *
     * @return PietanzaInOrdine
     */
    public function setQuantita($quantita)
    {
        $this->quantita = $quantita;

        return $this;
    }

    /**
     * Get quantita
     *
     * @return int
     */
    public function getQuantita()
    {
        return $this->quantita;
    }

    /**
     * Set ordine
     *
     * @param \AppBundle\Entity\Ordine $ordine
     *
     * @return PietanzaInOrdine
     */
    public function setOrdine(\AppBundle\Entity\Ordine $ordine)
    {
        $this->ordine = $ordine;

        return $this;
    }

    /**
     * Get ordine
     *
     * @return \AppBundle\Entity\Ordine
     */
    public function getOrdine()
    {
        return $this->ordine;
    }

    /**
     * Set pietanza
     *
     * @param \AppBundle\Entity\Pietanza $pietanza
     *
     * @return PietanzaInOrdine
     */
    public function setPietanza(\AppBundle\Entity\Pietanza $pietanza)
    {
        $this->pietanza = $pietanza;

        return $this;
    }

    /**
     * Get pietanza
     *
     * @return \AppBundle\Entity\Pietanza
     */
    public function getPietanza()
    {
        return $this->pietanza;
    }
}
