<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pietanza
 *
 * @ORM\Table(name="pietanza")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PietanzaRepository")
 */
class Pietanza
{
    /**
     * @var int
     *
     * @ORM\Column(name="codPietanza", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $codPietanza;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=100)
     */
    private $nome;

    /**
     * @var int
     *
     * @ORM\Column(name="calorie", type="integer")
     */
    private $calorie;

    /**
     * @var string
     *
     * @ORM\Column(name="descrizione", type="string", length=500, nullable=true)
     */
    private $descrizione;

    /**
     * @var float
     *
     * @ORM\Column(name="prezzo", type="float")
     */
    private $prezzo;

    /**
     * @var float
     *
     * @ORM\Column(name="grassi", type="float", nullable=true)
     */
    private $grassi;

    /**
     * @var float
     *
     * @ORM\Column(name="carboidrati", type="float", nullable=true)
     */
    private $carboidrati;

    /**
     * @var float
     *
     * @ORM\Column(name="zuccheri", type="float", nullable=true)
     */
    private $zuccheri;

    /**
     * @var float
     *
     * @ORM\Column(name="proteine", type="float", nullable=true)
     */
    private $proteine;

    /**
     * @var float
     *
     * @ORM\Column(name="sale", type="float", nullable=true)
     */
    private $sale;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Sezione", inversedBy="pietanze")
     */
    private $sezione;

    /**
     * @ORM\ManyToMany(targetEntity="Prodotto", inversedBy="pietanze")
     * @ORM\JoinTable(name="pietanze_ingredienti",
     *      joinColumns={@ORM\JoinColumn(name="codPietanza", referencedColumnName="codPietanza")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="codProdotto", referencedColumnName="id")}
     * )
     */
     protected $ingredienti;

     /**
      * @ORM\OneToMany(targetEntity="PietanzaInOrdine", mappedBy="pietanza")
      */
     protected $pietanzaInOrdini;

    /**
     * Get codPietanza
     *
     * @return int
     */
    public function getCodPietanza()
    {
        return $this->codPietanza;
    }

    /**
     * Set nome
     *
     * @param string $nome
     *
     * @return Pietanza
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set calorie
     *
     * @param integer $calorie
     *
     * @return Pietanza
     */
    public function setCalorie($calorie)
    {
        $this->calorie = $calorie;

        return $this;
    }

    /**
     * Get calorie
     *
     * @return int
     */
    public function getCalorie()
    {
        return $this->calorie;
    }

    /**
     * Set descrizione
     *
     * @param string $descrizione
     *
     * @return Pietanza
     */
    public function setDescrizione($descrizione)
    {
        $this->descrizione = $descrizione;

        return $this;
    }

    /**
     * Get descrizione
     *
     * @return string
     */
    public function getDescrizione()
    {
        return $this->descrizione;
    }

    /**
     * Set prezzo
     *
     * @param float $prezzo
     *
     * @return Pietanza
     */
    public function setPrezzo($prezzo)
    {
        $this->prezzo = $prezzo;

        return $this;
    }

    /**
     * Get prezzo
     *
     * @return float
     */
    public function getPrezzo()
    {
        return $this->prezzo;
    }

    /**
     * Set grassi
     *
     * @param float $grassi
     *
     * @return Pietanza
     */
    public function setGrassi($grassi)
    {
        $this->grassi = $grassi;

        return $this;
    }

    /**
     * Get grassi
     *
     * @return float
     */
    public function getGrassi()
    {
        return $this->grassi;
    }

    /**
     * Set carboidrati
     *
     * @param float $carboidrati
     *
     * @return Pietanza
     */
    public function setCarboidrati($carboidrati)
    {
        $this->carboidrati = $carboidrati;

        return $this;
    }

    /**
     * Get carboidrati
     *
     * @return float
     */
    public function getCarboidrati()
    {
        return $this->carboidrati;
    }

    /**
     * Set zuccheri
     *
     * @param float $zuccheri
     *
     * @return Pietanza
     */
    public function setZuccheri($zuccheri)
    {
        $this->zuccheri = $zuccheri;

        return $this;
    }

    /**
     * Get zuccheri
     *
     * @return float
     */
    public function getZuccheri()
    {
        return $this->zuccheri;
    }

    /**
     * Set proteine
     *
     * @param float $proteine
     *
     * @return Pietanza
     */
    public function setProteine($proteine)
    {
        $this->proteine = $proteine;

        return $this;
    }

    /**
     * Get proteine
     *
     * @return float
     */
    public function getProteine()
    {
        return $this->proteine;
    }

    /**
     * Set sale
     *
     * @param float $sale
     *
     * @return Pietanza
     */
    public function setSale($sale)
    {
        $this->sale = $sale;

        return $this;
    }

    /**
     * Get sale
     *
     * @return float
     */
    public function getSale()
    {
        return $this->sale;
    }

    /**
     * Set sezione
     *
     * @param \AppBundle\Entity\Sezione $sezione
     *
     * @return Pietanza
     */
    public function setSezione(\AppBundle\Entity\Sezione $sezione = null)
    {
        $this->sezione = $sezione;

        return $this;
    }

    /**
     * Get sezione
     *
     * @return \AppBundle\Entity\Sezione
     */
    public function getSezione()
    {
        return $this->sezione;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->ingredienti = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add ingredienti
     *
     * @param \AppBundle\Entity\Prodotto $ingredienti
     *
     * @return Pietanza
     */
    public function addIngredienti(\AppBundle\Entity\Prodotto $ingredienti)
    {
        $this->ingredienti[] = $ingredienti;

        return $this;
    }

    /**
     * Remove ingredienti
     *
     * @param \AppBundle\Entity\Prodotto $ingredienti
     */
    public function removeIngredienti(\AppBundle\Entity\Prodotto $ingredienti)
    {
        $this->ingredienti->removeElement($ingredienti);
    }

    /**
     * Get ingredienti
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIngredienti()
    {
        return $this->ingredienti;
    }

    /**
     * Add pietanzaInOrdini
     *
     * @param \AppBundle\Entity\PietanzaInOrdine $pietanzaInOrdini
     *
     * @return Pietanza
     */
    public function addPietanzaInOrdini(\AppBundle\Entity\PietanzaInOrdine $pietanzaInOrdini)
    {
        $this->pietanzaInOrdini[] = $pietanzaInOrdini;

        return $this;
    }

    /**
     * Remove pietanzaInOrdini
     *
     * @param \AppBundle\Entity\PietanzaInOrdine $pietanzaInOrdini
     */
    public function removePietanzaInOrdini(\AppBundle\Entity\PietanzaInOrdine $pietanzaInOrdini)
    {
        $this->pietanzaInOrdini->removeElement($pietanzaInOrdini);
    }

    /**
     * Get pietanzaInOrdini
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPietanzaInOrdini()
    {
        return $this->pietanzaInOrdini;
    }
}
