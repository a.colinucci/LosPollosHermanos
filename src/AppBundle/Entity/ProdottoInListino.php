<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProdottoInListino
 *
 * @ORM\Table(name="prodotto_in_listino")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProdottoInListinoRepository")
 */
class ProdottoInListino
{
    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Fornitore", inversedBy="prodottiInListino")
     * @ORM\JoinColumn(name="fornitore_id", referencedColumnName="id", nullable=FALSE)
     */
    protected $fornitore;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Prodotto", inversedBy="prodottoInListini")
     * @ORM\JoinColumn(name="prodotto_id", referencedColumnName="id", nullable=FALSE)
     */
    protected $prodotto;

    /**
     * @var float
     *
     * @ORM\Column(name="prezzo", type="float")
     */
    private $prezzo;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set prezzo
     *
     * @param float $prezzo
     *
     * @return ProdottoInListino
     */
    public function setPrezzo($prezzo)
    {
        $this->prezzo = $prezzo;

        return $this;
    }

    /**
     * Get prezzo
     *
     * @return float
     */
    public function getPrezzo()
    {
        return $this->prezzo;
    }

    /**
     * Set fornitore
     *
     * @param \AppBundle\Entity\Fornitore $fornitore
     *
     * @return ProdottoInListino
     */
    public function setFornitore(\AppBundle\Entity\Fornitore $fornitore)
    {
        $this->fornitore = $fornitore;

        return $this;
    }

    /**
     * Get fornitore
     *
     * @return \AppBundle\Entity\Fornitore
     */
    public function getFornitore()
    {
        return $this->fornitore;
    }

    /**
     * Set prodotto
     *
     * @param \AppBundle\Entity\Prodotto $prodotto
     *
     * @return ProdottoInListino
     */
    public function setProdotto(\AppBundle\Entity\Prodotto $prodotto)
    {
        $this->prodotto = $prodotto;

        return $this;
    }

    /**
     * Get prodotto
     *
     * @return \AppBundle\Entity\Prodotto
     */
    public function getProdotto()
    {
        return $this->prodotto;
    }
}
