<?php

namespace AppBundle\Entity;

use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Doctrine\ORM\Mapping as ORM;

/**
 * GiornoApertura
 *
 * @ORM\Table(name="giorno_apertura")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\GiornoAperturaRepository")
 * @ExclusionPolicy("all")
 */
class GiornoApertura
{
    /**
     * @var \DateTime
     * @ORM\Id
     * @ORM\Column(name="giorno", type="string")
     * @Expose
     */
    private $giorno;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Ristorante", inversedBy="giorniApertura")
     */
    private $ristorante;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="orarioAperturaPranzo", type="time", nullable=true)
     * @Expose
     */
    private $orarioAperturaPranzo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="orarioChiusuraPranzo", type="time", nullable=true)
     * @Expose
     */
    private $orarioChiusuraPranzo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="orarioAperturaCena", type="time", nullable=true)
     * @Expose
     */
    private $orarioAperturaCena;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="orarioChiusuraCena", type="time", nullable=true)
     * @Expose
     */
    private $orarioChiusuraCena;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Turno", mappedBy="giornoApertura")
     **/
    private $turni;

    /**
     * Set giorno
     *
     * @param string $giorno
     *
     * @return GiornoApertura
     */
    public function setGiorno($giorno)
    {
        $this->giorno = $giorno;

        return $this;
    }

    /**
     * Get giorno
     *
     * @return string
     */
    public function getGiorno()
    {
        return $this->giorno;
    }

    /**
     * Set orarioAperturaPranzo
     *
     * @param \DateTime $orarioAperturaPranzo
     *
     * @return GiornoApertura
     */
    public function setOrarioAperturaPranzo($orarioAperturaPranzo)
    {
        $this->orarioAperturaPranzo = $orarioAperturaPranzo;

        return $this;
    }

    /**
     * Get orarioAperturaPranzo
     *
     * @return \DateTime
     */
    public function getOrarioAperturaPranzo()
    {
        return $this->orarioAperturaPranzo;
    }

    /**
     * Set orarioChiusuraPranzo
     *
     * @param \DateTime $orarioChiusuraPranzo
     *
     * @return GiornoApertura
     */
    public function setOrarioChiusuraPranzo($orarioChiusuraPranzo)
    {
        $this->orarioChiusuraPranzo = $orarioChiusuraPranzo;

        return $this;
    }

    /**
     * Get orarioChiusuraPranzo
     *
     * @return \DateTime
     */
    public function getOrarioChiusuraPranzo()
    {
        return $this->orarioChiusuraPranzo;
    }

    /**
     * Set orarioAperturaCena
     *
     * @param \DateTime $orarioAperturaCena
     *
     * @return GiornoApertura
     */
    public function setOrarioAperturaCena($orarioAperturaCena)
    {
        $this->orarioAperturaCena = $orarioAperturaCena;

        return $this;
    }

    /**
     * Get orarioAperturaCena
     *
     * @return \DateTime
     */
    public function getOrarioAperturaCena()
    {
        return $this->orarioAperturaCena;
    }

    /**
     * Set orarioChiusuraCena
     *
     * @param \DateTime $orarioChiusuraCena
     *
     * @return GiornoApertura
     */
    public function setOrarioChiusuraCena($orarioChiusuraCena)
    {
        $this->orarioChiusuraCena = $orarioChiusuraCena;

        return $this;
    }

    /**
     * Get orarioChiusuraCena
     *
     * @return \DateTime
     */
    public function getOrarioChiusuraCena()
    {
        return $this->orarioChiusuraCena;
    }

    /**
     * Set ristorante
     *
     * @param \AppBundle\Entity\Ristorante $ristorante
     *
     * @return GiornoApertura
     */
    public function setRistorante(\AppBundle\Entity\Ristorante $ristorante)
    {
        $this->ristorante = $ristorante;

        return $this;
    }

    /**
     * Get ristorante
     *
     * @return \AppBundle\Entity\Ristorante
     */
    public function getRistorante()
    {
        return $this->ristorante;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->turni = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add turni
     *
     * @param \AppBundle\Entity\Turno $turni
     *
     * @return GiornoApertura
     */
    public function addTurni(\AppBundle\Entity\Turno $turni)
    {
        $this->turni[] = $turni;

        return $this;
    }

    /**
     * Remove turni
     *
     * @param \AppBundle\Entity\Turno $turni
     */
    public function removeTurni(\AppBundle\Entity\Turno $turni)
    {
        $this->turni->removeElement($turni);
    }

    /**
     * Get turni
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTurni()
    {
        return $this->turni;
    }
}
