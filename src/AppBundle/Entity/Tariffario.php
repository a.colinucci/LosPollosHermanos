<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tariffario
 *
 * @ORM\Table(name="tariffario")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TariffarioRepository")
 */
class Tariffario
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(name="cap", type="integer")
     */
    private $cap;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Ristorante", inversedBy="tariffari")
     * @ORM\JoinColumn(name="codRistorante", referencedColumnName="id")
     */
    private $ristorante;

    /**
     * @var int
     *
     * @ORM\Column(name="prezzo", type="integer")
     */
    private $prezzo;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Ordine", mappedBy="tariffa")
     **/
    private $ordini;


    /**
     * Set codZona
     *
     * @param integer $cap
     *
     * @return Tariffario
     */
    public function setCodZona($cap)
    {
        $this->cap = $cap;

        return $this;
    }

    /**
     * Get cap
     *
     * @return integer
     */
    public function getCap()
    {
        return $this->cap;
    }

    /**
     * Set prezzo
     *
     * @param integer $prezzo
     *
     * @return Tariffario
     */
    public function setPrezzo($prezzo)
    {
        $this->prezzo = $prezzo;

        return $this;
    }

    /**
     * Get prezzo
     *
     * @return integer
     */
    public function getPrezzo()
    {
        return $this->prezzo;
    }

    /**
     * Set ristorante
     *
     * @param \AppBundle\Entity\Ristorante $ristorante
     *
     * @return Tariffario
     */
    public function setRistorante(\AppBundle\Entity\Ristorante $ristorante)
    {
        $this->ristorante = $ristorante;

        return $this;
    }

    /**
     * Get ristorante
     *
     * @return \AppBundle\Entity\Ristorante
     */
    public function getRistorante()
    {
        return $this->ristorante;
    }

    /**
     * Set cap
     *
     * @param integer $cap
     *
     * @return Tariffario
     */
    public function setCap($cap)
    {
        $this->cap = $cap;

        return $this;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->ordini = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add ordini
     *
     * @param \AppBundle\Entity\Ordine $ordini
     *
     * @return Tariffario
     */
    public function addOrdini(\AppBundle\Entity\Ordine $ordini)
    {
        $this->ordini[] = $ordini;

        return $this;
    }

    /**
     * Remove ordini
     *
     * @param \AppBundle\Entity\Ordine $ordini
     */
    public function removeOrdini(\AppBundle\Entity\Ordine $ordini)
    {
        $this->ordini->removeElement($ordini);
    }

    /**
     * Get ordini
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrdini()
    {
        return $this->ordini;
    }
}
