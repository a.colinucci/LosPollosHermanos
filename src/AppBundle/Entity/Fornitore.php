<?php

namespace AppBundle\Entity;

use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Doctrine\ORM\Mapping as ORM;

/**
 * Fornitore
 *
 * @ORM\Table(name="fornitore")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FornitoreRepository")
 * @ExclusionPolicy("all")
 */
class Fornitore
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nomeAzienda", type="string", length=100)
     * @Expose
     */
    private $nomeAzienda;

    /**
     * @var string
     *
     * @ORM\Column(name="indirizzo", type="string", length=100)
     */
    private $indirizzo;

    /**
     * @var string
     *
     * @ORM\Column(name="citta", type="string", length=100)
     */
    private $citta;

    /**
     * @var string
     *
     * @ORM\Column(name="cellulare", type="string", length=50)
     */
    private $cellulare;

    /**
     * @var string
     *
     * @ORM\Column(name="partitaIVA", type="string", length=11, unique=true)
     */
    private $partitaIVA;

    /**
     * @var ArrayCollection
     * @ORM\OneToOne(targetEntity="User", mappedBy="fornitore")
     **/
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="ProdottoInListino", mappedBy="fornitore")
     * @Expose
     */
    protected $prodottiInListino;

    /**
     * @ORM\OneToMany(targetEntity="Fornitura", mappedBy="fornitore")
     */
    protected $forniture;

    /**
     * @ORM\ManyToMany(targetEntity="Ristorante", inversedBy="fornitori")
     * @ORM\JoinTable(name="fornitore_ristorante",
     *      joinColumns={@ORM\JoinColumn(name="fornitore_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="ristorante_id", referencedColumnName="id")}
     * )
     */
     protected $ristoranti;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nomeAzienda
     *
     * @param string $nomeAzienda
     *
     * @return Fornitore
     */
    public function setNomeAzienda($nomeAzienda)
    {
        $this->nomeAzienda = $nomeAzienda;

        return $this;
    }

    /**
     * Get nomeAzienda
     *
     * @return string
     */
    public function getNomeAzienda()
    {
        return $this->nomeAzienda;
    }

    /**
     * Set indirizzo
     *
     * @param string $indirizzo
     *
     * @return Fornitore
     */
    public function setIndirizzo($indirizzo)
    {
        $this->indirizzo = $indirizzo;

        return $this;
    }

    /**
     * Get indirizzo
     *
     * @return string
     */
    public function getIndirizzo()
    {
        return $this->indirizzo;
    }

    /**
     * Set partitaIVA
     *
     * @param string $partitaIVA
     *
     * @return Fornitore
     */
    public function setPartitaIVA($partitaIVA)
    {
        $this->partitaIVA = $partitaIVA;

        return $this;
    }

    /**
     * Get partitaIVA
     *
     * @return string
     */
    public function getPartitaIVA()
    {
        return $this->partitaIVA;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Fornitore
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set cellulare
     *
     * @param string $cellulare
     *
     * @return Fornitore
     */
    public function setCellulare($cellulare)
    {
        $this->cellulare = $cellulare;

        return $this;
    }

    /**
     * Get cellulare
     *
     * @return string
     */
    public function getCellulare()
    {
        return $this->cellulare;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->prodottiInListino = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add prodottiInListino
     *
     * @param \AppBundle\Entity\ProdottoInListino $prodottiInListino
     *
     * @return Fornitore
     */
    public function addProdottiInListino(\AppBundle\Entity\ProdottoInListino $prodottiInListino)
    {
        $this->prodottiInListino[] = $prodottiInListino;

        return $this;
    }

    /**
     * Remove prodottiInListino
     *
     * @param \AppBundle\Entity\ProdottoInListino $prodottiInListino
     */
    public function removeProdottiInListino(\AppBundle\Entity\ProdottoInListino $prodottiInListino)
    {
        $this->prodottiInListino->removeElement($prodottiInListino);
    }

    /**
     * Get prodottiInListino
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProdottiInListino()
    {
        return $this->prodottiInListino;
    }

    /**
     * Add forniture
     *
     * @param \AppBundle\Entity\Fornitura $forniture
     *
     * @return Fornitore
     */
    public function addForniture(\AppBundle\Entity\Fornitura $forniture)
    {
        $this->forniture[] = $forniture;

        return $this;
    }

    /**
     * Remove forniture
     *
     * @param \AppBundle\Entity\Fornitura $forniture
     */
    public function removeForniture(\AppBundle\Entity\Fornitura $forniture)
    {
        $this->forniture->removeElement($forniture);
    }

    /**
     * Get forniture
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getForniture()
    {
        return $this->forniture;
    }

    /**
     * Add ristoranti
     *
     * @param \AppBundle\Entity\Ristorante $ristoranti
     *
     * @return Fornitore
     */
    public function addRistoranti(\AppBundle\Entity\Ristorante $ristoranti)
    {
        $this->ristoranti[] = $ristoranti;

        return $this;
    }

    /**
     * Remove ristoranti
     *
     * @param \AppBundle\Entity\Ristorante $ristoranti
     */
    public function removeRistoranti(\AppBundle\Entity\Ristorante $ristoranti)
    {
        $this->ristoranti->removeElement($ristoranti);
    }

    /**
     * Get ristoranti
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRistoranti()
    {
        return $this->ristoranti;
    }

    public function clearRistoranti()
    {
        $this->getRistoranti()->clear();
    }

    /**
     * Set citta
     *
     * @param string $citta
     *
     * @return Fornitore
     */
    public function setCitta($citta)
    {
        $this->citta = $citta;

        return $this;
    }

    /**
     * Get citta
     *
     * @return string
     */
    public function getCitta()
    {
        return $this->citta;
    }
}
