<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Menu
 *
 * @ORM\Table(name="menu")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MenuRepository")
 */
class Menu
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="descrizione", type="string", length=500, nullable=true)
     */
    private $descrizione;

    /**
     * One Ristorante has One Menu.
     * @ORM\OneToOne(targetEntity="Ristorante", inversedBy="menu")
     * @ORM\JoinColumn(name="ristorante_id", referencedColumnName="id")
     */
    private $ristorante;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Sezione", mappedBy="menu")
     **/
    private $sezioni;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ristorante
     *
     * @param \AppBundle\Entity\Ristorante $ristorante
     *
     * @return Menu
     */
    public function setRistorante(\AppBundle\Entity\Ristorante $ristorante = null)
    {
        $this->ristorante = $ristorante;

        return $this;
    }

    /**
     * Get ristorante
     *
     * @return \AppBundle\Entity\Ristorante
     */
    public function getRistorante()
    {
        return $this->ristorante;
    }

    /**
     * Set descrizione
     *
     * @param string $descrizione
     *
     * @return Menu
     */
    public function setDescrizione($descrizione)
    {
        $this->descrizione = $descrizione;

        return $this;
    }

    /**
     * Get descrizione
     *
     * @return string
     */
    public function getDescrizione()
    {
        return $this->descrizione;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->sezioni = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add sezioni
     *
     * @param \AppBundle\Entity\Sezione $sezioni
     *
     * @return Menu
     */
    public function addSezioni(\AppBundle\Entity\Sezione $sezioni)
    {
        $this->sezioni[] = $sezioni;

        return $this;
    }

    /**
     * Remove sezioni
     *
     * @param \AppBundle\Entity\Sezione $sezioni
     */
    public function removeSezioni(\AppBundle\Entity\Sezione $sezioni)
    {
        $this->sezioni->removeElement($sezioni);
    }

    /**
     * Get sezioni
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSezioni()
    {
        return $this->sezioni;
    }

    public function clearSezioni()
    {
        $this->getSezioni()->clear();
    }
}
