<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Prodotto
 *
 * @ORM\Table(name="prodotto")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProdottoRepository")
 */
class Prodotto
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=30, unique=true)
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(name="descrizione", type="string", length=500, nullable=true)
     */
    private $descrizione;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo", type="string", length=50)
     */
    private $tipo;

    /**
     * @ORM\OneToMany(targetEntity="ProdottoInListino", mappedBy="prodotto")
     */
    protected $prodottoInListini;

    /**
     * @ORM\OneToMany(targetEntity="ProdottoInMagazzino", mappedBy="prodotto")
     */
    protected $prodottoInMagazzini;

    /**
     * @ORM\OneToMany(targetEntity="Fornitura", mappedBy="prodotto")
     */
    protected $forniture;

    /**
     * @ORM\ManyToMany(targetEntity="Pietanza", mappedBy="ingredienti")
     */
    private $pietanze;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nome
     *
     * @param string $nome
     *
     * @return Prodotto
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set descrizione
     *
     * @param string $descrizione
     *
     * @return Prodotto
     */
    public function setDescrizione($descrizione)
    {
        $this->descrizione = $descrizione;

        return $this;
    }

    /**
     * Get descrizione
     *
     * @return string
     */
    public function getDescrizione()
    {
        return $this->descrizione;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     *
     * @return Prodotto
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->prodottoInListini = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add prodottoInListini
     *
     * @param \AppBundle\Entity\ProdottoInListino $prodottoInListini
     *
     * @return Prodotto
     */
    public function addProdottoInListini(\AppBundle\Entity\ProdottoInListino $prodottoInListini)
    {
        $this->prodottoInListini[] = $prodottoInListini;

        return $this;
    }

    /**
     * Remove prodottoInListini
     *
     * @param \AppBundle\Entity\ProdottoInListino $prodottoInListini
     */
    public function removeProdottoInListini(\AppBundle\Entity\ProdottoInListino $prodottoInListini)
    {
        $this->prodottoInListini->removeElement($prodottoInListini);
    }

    /**
     * Get prodottoInListini
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProdottoInListini()
    {
        return $this->prodottoInListini;
    }


    /**
     * Add forniture
     *
     * @param \AppBundle\Entity\Fornitura $forniture
     *
     * @return Prodotto
     */
    public function addForniture(\AppBundle\Entity\Fornitura $forniture)
    {
        $this->forniture[] = $forniture;

        return $this;
    }

    /**
     * Remove forniture
     *
     * @param \AppBundle\Entity\Fornitura $forniture
     */
    public function removeForniture(\AppBundle\Entity\Fornitura $forniture)
    {
        $this->forniture->removeElement($forniture);
    }

    /**
     * Get forniture
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getForniture()
    {
        return $this->forniture;
    }

    /**
     * Add prodottoInMagazzini
     *
     * @param \AppBundle\Entity\ProdottoInMagazzino $prodottoInMagazzini
     *
     * @return Prodotto
     */
    public function addProdottoInMagazzini(\AppBundle\Entity\ProdottoInMagazzino $prodottoInMagazzini)
    {
        $this->prodottoInMagazzini[] = $prodottoInMagazzini;

        return $this;
    }

    /**
     * Remove prodottoInMagazzini
     *
     * @param \AppBundle\Entity\ProdottoInMagazzino $prodottoInMagazzini
     */
    public function removeProdottoInMagazzini(\AppBundle\Entity\ProdottoInMagazzino $prodottoInMagazzini)
    {
        $this->prodottoInMagazzini->removeElement($prodottoInMagazzini);
    }

    /**
     * Get prodottoInMagazzini
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProdottoInMagazzini()
    {
        return $this->prodottoInMagazzini;
    }

    /**
     * Add pietanze
     *
     * @param \AppBundle\Entity\Pietanza $pietanze
     *
     * @return Prodotto
     */
    public function addPietanze(\AppBundle\Entity\Pietanza $pietanze)
    {
        $this->pietanze[] = $pietanze;

        return $this;
    }

    /**
     * Remove pietanze
     *
     * @param \AppBundle\Entity\Pietanza $pietanze
     */
    public function removePietanze(\AppBundle\Entity\Pietanza $pietanze)
    {
        $this->pietanze->removeElement($pietanze);
    }

    /**
     * Get pietanze
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPietanze()
    {
        return $this->pietanze;
    }
}
