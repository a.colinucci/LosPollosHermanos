<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cliente
 *
 * @ORM\Table(name="cliente")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ClienteRepository")
 */
class Cliente
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=30)
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(name="cognome", type="string", length=50)
     */
    private $cognome;

    /**
     * @var string
     *
     * @ORM\Column(name="cellulare", type="string", length=50)
     */
    private $cellulare;

    /**
     * @var string
     *
     * @ORM\Column(name="indirizzo", type="string", length=100)
     */
    private $indirizzo;

    /**
     * @var string
     *
     * @ORM\Column(name="citta", type="string", length=100)
     */
    private $citta;

    /**
     * @var string
     *
     * @ORM\Column(name="numeroCarta", type="string", unique=true, length=16)
     */
    private $numeroCarta;

    /**
     * @var string
     *
     * @ORM\Column(name="meseScadenzaCarta", type="string", length=20)
     */
    private $meseScadenzaCarta;

    /**
     * @var int
     *
     * @ORM\Column(name="annoScadenzaCarta", type="integer")
     */
    private $annoScadenzaCarta;

    /**
     * @var int
     *
     * @ORM\Column(name="cvvCarta", type="integer")
     */
    private $cvvCarta;

    /**
     * @var ArrayCollection
     * @ORM\OneToOne(targetEntity="User", mappedBy="cliente")
     **/
    private $user;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Prenotazione", mappedBy="cliente")
     **/
    private $prenotazioni;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Ordine", mappedBy="cliente")
     **/
    private $ordini;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nome
     *
     * @param string $nome
     *
     * @return Cliente
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set cognome
     *
     * @param string $cognome
     *
     * @return Cliente
     */
    public function setCognome($cognome)
    {
        $this->cognome = $cognome;

        return $this;
    }

    /**
     * Get cognome
     *
     * @return string
     */
    public function getCognome()
    {
        return $this->cognome;
    }

    /**
     * Set cellulare
     *
     * @param string $cellulare
     *
     * @return Cliente
     */
    public function setCellulare($cellulare)
    {
        $this->cellulare = $cellulare;

        return $this;
    }

    /**
     * Get cellulare
     *
     * @return string
     */
    public function getCellulare()
    {
        return $this->cellulare;
    }

    /**
     * Set indirizzo
     *
     * @param string $indirizzo
     *
     * @return Cliente
     */
    public function setIndirizzo($indirizzo)
    {
        $this->indirizzo = $indirizzo;

        return $this;
    }

    /**
     * Get indirizzo
     *
     * @return string
     */
    public function getIndirizzo()
    {
        return $this->indirizzo;
    }

    /**
     * Set meseScadenzaCarta
     *
     * @param string $meseScadenzaCarta
     *
     * @return Cliente
     */
    public function setMeseScadenzaCarta($meseScadenzaCarta)
    {
        $this->meseScadenzaCarta = $meseScadenzaCarta;

        return $this;
    }

    /**
     * Get meseScadenzaCarta
     *
     * @return string
     */
    public function getMeseScadenzaCarta()
    {
        return $this->meseScadenzaCarta;
    }

    /**
     * Set annoScadenzaCarta
     *
     * @param integer $annoScadenzaCarta
     *
     * @return Cliente
     */
    public function setAnnoScadenzaCarta($annoScadenzaCarta)
    {
        $this->annoScadenzaCarta = $annoScadenzaCarta;

        return $this;
    }

    /**
     * Get annoScadenzaCarta
     *
     * @return integer
     */
    public function getAnnoScadenzaCarta()
    {
        return $this->annoScadenzaCarta;
    }

    /**
     * Set cvvCarta
     *
     * @param integer $cvvCarta
     *
     * @return Cliente
     */
    public function setCvvCarta($cvvCarta)
    {
        $this->cvvCarta = $cvvCarta;

        return $this;
    }

    /**
     * Get cvvCarta
     *
     * @return integer
     */
    public function getCvvCarta()
    {
        return $this->cvvCarta;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Cliente
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->prenotazioni = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add prenotazioni
     *
     * @param \AppBundle\Entity\Prenotazione $prenotazioni
     *
     * @return Cliente
     */
    public function addPrenotazioni(\AppBundle\Entity\Prenotazione $prenotazioni)
    {
        $this->prenotazioni[] = $prenotazioni;

        return $this;
    }

    /**
     * Remove prenotazioni
     *
     * @param \AppBundle\Entity\Prenotazione $prenotazioni
     */
    public function removePrenotazioni(\AppBundle\Entity\Prenotazione $prenotazioni)
    {
        $this->prenotazioni->removeElement($prenotazioni);
    }

    /**
     * Get prenotazioni
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPrenotazioni()
    {
        return $this->prenotazioni;
    }

    /**
     * Set citta
     *
     * @param string $citta
     *
     * @return Cliente
     */
    public function setCitta($citta)
    {
        $this->citta = $citta;

        return $this;
    }

    /**
     * Get citta
     *
     * @return string
     */
    public function getCitta()
    {
        return $this->citta;
    }

    /**
     * Add ordini
     *
     * @param \AppBundle\Entity\Ordine $ordini
     *
     * @return Cliente
     */
    public function addOrdini(\AppBundle\Entity\Ordine $ordini)
    {
        $this->ordini[] = $ordini;

        return $this;
    }

    /**
     * Remove ordini
     *
     * @param \AppBundle\Entity\Ordine $ordini
     */
    public function removeOrdini(\AppBundle\Entity\Ordine $ordini)
    {
        $this->ordini->removeElement($ordini);
    }

    /**
     * Get ordini
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrdini()
    {
        return $this->ordini;
    }

    /**
     * Set numeroCarta
     *
     * @param string $numeroCarta
     *
     * @return Cliente
     */
    public function setNumeroCarta($numeroCarta)
    {
        $this->numeroCarta = $numeroCarta;

        return $this;
    }

    /**
     * Get numeroCarta
     *
     * @return string
     */
    public function getNumeroCarta()
    {
        return $this->numeroCarta;
    }
}
