<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Recensione
 *
 * @ORM\Table(name="recensione")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RecensioneRepository")
 */
class Recensione
{
    /**
     * @var int
     *
     * @ORM\Column(name="codRecensione", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $codRecensione;

    /**
     * @var int
     *
     * @ORM\Column(name="voto", type="integer")
     */
    private $voto;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data", type="datetime")
     */
    private $data;

    /**
     * @var string
     *
     * @ORM\Column(name="titolo", type="string", length=100)
     */
    private $titolo;

    /**
     * @var string
     *
     * @ORM\Column(name="testo", type="string", length=2000)
     */
    private $testo;

    /**
     * @var ArrayCollection
     * @ORM\OneToOne(targetEntity="Ordine")
     * @ORM\JoinColumn(name="codOrdine", referencedColumnName="codOrdine")
     **/
    private $ordine;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Ristorante", inversedBy="recensioni")
     */
    private $ristorante;


    /**
     * Get codRecensione
     *
     * @return int
     */
    public function getCodRecensione()
    {
        return $this->codRecensione;
    }

    /**
     * Set voto
     *
     * @param integer $voto
     *
     * @return Recensione
     */
    public function setVoto($voto)
    {
        $this->voto = $voto;

        return $this;
    }

    /**
     * Get voto
     *
     * @return int
     */
    public function getVoto()
    {
        return $this->voto;
    }

    /**
     * Set data
     *
     * @param \DateTime $data
     *
     * @return Recensione
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return \DateTime
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set testo
     *
     * @param string $testo
     *
     * @return Recensione
     */
    public function setTesto($testo)
    {
        $this->testo = $testo;

        return $this;
    }

    /**
     * Get testo
     *
     * @return string
     */
    public function getTesto()
    {
        return $this->testo;
    }

    /**
     * Set ordine
     *
     * @param \AppBundle\Entity\Ordine $ordine
     *
     * @return Recensione
     */
    public function setOrdine(\AppBundle\Entity\Ordine $ordine = null)
    {
        $this->ordine = $ordine;

        return $this;
    }

    /**
     * Get ordine
     *
     * @return \AppBundle\Entity\Ordine
     */
    public function getOrdine()
    {
        return $this->ordine;
    }

    /**
     * Set ristorante
     *
     * @param \AppBundle\Entity\Ristorante $ristorante
     *
     * @return Recensione
     */
    public function setRistorante(\AppBundle\Entity\Ristorante $ristorante = null)
    {
        $this->ristorante = $ristorante;

        return $this;
    }

    /**
     * Get ristorante
     *
     * @return \AppBundle\Entity\Ristorante
     */
    public function getRistorante()
    {
        return $this->ristorante;
    }

    /**
     * Set titolo
     *
     * @param string $titolo
     *
     * @return Recensione
     */
    public function setTitolo($titolo)
    {
        $this->titolo = $titolo;

        return $this;
    }

    /**
     * Get titolo
     *
     * @return string
     */
    public function getTitolo()
    {
        return $this->titolo;
    }
}
