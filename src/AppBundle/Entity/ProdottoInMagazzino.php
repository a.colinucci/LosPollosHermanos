<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProdottoInMagazzino
 *
 * @ORM\Table(name="prodotto_in_magazzino")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProdottoInMagazzinoRepository")
 */
class ProdottoInMagazzino
{

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Ristorante", inversedBy="prodottiInMagazzino")
     * @ORM\JoinColumn(name="codRistorante", referencedColumnName="id", nullable=FALSE)
     */
    protected $ristorante;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Prodotto", inversedBy="prodottoInMagazzini")
     * @ORM\JoinColumn(name="codProdotto", referencedColumnName="id", nullable=FALSE)
     */
    protected $prodotto;

    /**
     * @var int
     *
     * @ORM\Column(name="quantita", type="integer")
     */
    private $quantita;

    /**
     * Set quantita
     *
     * @param integer $quantita
     *
     * @return ProdottoInMagazzino
     */
    public function setQuantita($quantita)
    {
        $this->quantita = $quantita;

        return $this;
    }

    /**
     * Get quantita
     *
     * @return int
     */
    public function getQuantita()
    {
        return $this->quantita;
    }

    /**
     * Set ristorante
     *
     * @param \AppBundle\Entity\Ristorante $ristorante
     *
     * @return ProdottoInMagazzino
     */
    public function setRistorante(\AppBundle\Entity\Ristorante $ristorante)
    {
        $this->ristorante = $ristorante;

        return $this;
    }

    /**
     * Get ristorante
     *
     * @return \AppBundle\Entity\Ristorante
     */
    public function getRistorante()
    {
        return $this->ristorante;
    }

    /**
     * Set prodotto
     *
     * @param \AppBundle\Entity\Prodotto $prodotto
     *
     * @return ProdottoInMagazzino
     */
    public function setProdotto(\AppBundle\Entity\Prodotto $prodotto)
    {
        $this->prodotto = $prodotto;

        return $this;
    }

    /**
     * Get prodotto
     *
     * @return \AppBundle\Entity\Prodotto
     */
    public function getProdotto()
    {
        return $this->prodotto;
    }
}
