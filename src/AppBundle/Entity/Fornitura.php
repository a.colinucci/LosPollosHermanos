<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Fornitura
 *
 * @ORM\Table(name="fornitura")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FornituraRepository")
 */
class Fornitura
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dataRichiesta", type="datetime")
     */
    private $dataRichiesta;

    /**
     * @var float
     *
     * @ORM\Column(name="prezzo", type="float")
     */
    private $prezzo;

    /**
     * @var int
     *
     * @ORM\Column(name="quantita", type="integer")
     */
    private $quantita;

    /**
     * @ORM\ManyToOne(targetEntity="Ristorante", inversedBy="forniture")
     * @ORM\JoinColumn(name="ristorante_id", referencedColumnName="id", nullable=FALSE)
     */
    protected $ristorante;

    /**
     * @ORM\ManyToOne(targetEntity="Fornitore", inversedBy="forniture")
     * @ORM\JoinColumn(name="fornitore_id", referencedColumnName="id", nullable=FALSE)
     */
    protected $fornitore;

    /**
     * @ORM\ManyToOne(targetEntity="Prodotto", inversedBy="forniture")
     * @ORM\JoinColumn(name="prodotto_id", referencedColumnName="id", nullable=FALSE)
     */
    protected $prodotto;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dataRichiesta
     *
     * @param \DateTime $dataRichiesta
     *
     * @return Fornitura
     */
    public function setDataRichiesta($dataRichiesta)
    {
        $this->dataRichiesta = $dataRichiesta;

        return $this;
    }

    /**
     * Get dataRichiesta
     *
     * @return \DateTime
     */
    public function getDataRichiesta()
    {
        return $this->dataRichiesta;
    }

    /**
     * Set prezzo
     *
     * @param float $prezzo
     *
     * @return Fornitura
     */
    public function setPrezzo($prezzo)
    {
        $this->prezzo = $prezzo;

        return $this;
    }

    /**
     * Get prezzo
     *
     * @return float
     */
    public function getPrezzo()
    {
        return $this->prezzo;
    }

    /**
     * Set ristorante
     *
     * @param \AppBundle\Entity\Ristorante $ristorante
     *
     * @return Fornitura
     */
    public function setRistorante(\AppBundle\Entity\Ristorante $ristorante = null)
    {
        $this->ristorante = $ristorante;

        return $this;
    }

    /**
     * Get ristorante
     *
     * @return \AppBundle\Entity\Ristorante
     */
    public function getRistorante()
    {
        return $this->ristorante;
    }

    /**
     * Set fornitore
     *
     * @param \AppBundle\Entity\Fornitore $fornitore
     *
     * @return Fornitura
     */
    public function setFornitore(\AppBundle\Entity\Fornitore $fornitore)
    {
        $this->fornitore = $fornitore;

        return $this;
    }

    /**
     * Get fornitore
     *
     * @return \AppBundle\Entity\Fornitore
     */
    public function getFornitore()
    {
        return $this->fornitore;
    }


    /**
     * Set quantita
     *
     * @param integer $quantita
     *
     * @return Fornitura
     */
    public function setQuantita($quantita)
    {
        $this->quantita = $quantita;

        return $this;
    }

    /**
     * Get quantita
     *
     * @return integer
     */
    public function getQuantita()
    {
        return $this->quantita;
    }

    /**
     * Set prodotto
     *
     * @param \AppBundle\Entity\Prodotto $prodotto
     *
     * @return Fornitura
     */
    public function setProdotto(\AppBundle\Entity\Prodotto $prodotto)
    {
        $this->prodotto = $prodotto;

        return $this;
    }

    /**
     * Get prodotto
     *
     * @return \AppBundle\Entity\Prodotto
     */
    public function getProdotto()
    {
        return $this->prodotto;
    }
}
