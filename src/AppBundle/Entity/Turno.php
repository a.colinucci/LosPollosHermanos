<?php

namespace AppBundle\Entity;

use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Doctrine\ORM\Mapping as ORM;

/**
 * Turno
 *
 * @ORM\Table(name="turno")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TurnoRepository")
 * @ExclusionPolicy("all")
 */
class Turno
{
    /**
     * @var int
     *
     * @ORM\Column(name="codTurno", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $codTurno;

    /**
     * @var string
     * @ORM\Column(name="fascia", type="string", length=30)
     * @Expose
     */
    private $fascia;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="GiornoApertura", inversedBy="turni")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="giorno", referencedColumnName="giorno", nullable=false),
     *   @ORM\JoinColumn(name="codRistorante", referencedColumnName="ristorante_id", nullable=false)
     * })
     */
    private $giornoApertura;

    /**
     * @Expose
     * @ORM\ManyToOne(targetEntity="Dipendente", inversedBy="turni")
     * @ORM\JoinColumn(name="codDipendente", referencedColumnName="codDipendente", nullable=FALSE)
     */
    protected $dipendente;

    /**
     * @var \DateTime
     *
     * @Expose
     * @ORM\Column(name="oraInizio", type="time")
     */
    private $oraInizio;

    /**
     * @var \DateTime
     *
     * @Expose
     * @ORM\Column(name="oraFine", type="time")
     */
    private $oraFine;

    /**
     * Set oraFine
     *
     * @param \DateTime $oraFine
     *
     * @return Turno
     */
    public function setOraFine($oraFine)
    {
        $this->oraFine = $oraFine;

        return $this;
    }

    /**
     * Get oraFine
     *
     * @return \DateTime
     */
    public function getOraFine()
    {
        return $this->oraFine;
    }

    /**
     * Set fascia
     *
     * @param string $fascia
     *
     * @return Turno
     */
    public function setFascia($fascia)
    {
        $this->fascia = $fascia;

        return $this;
    }

    /**
     * Get fascia
     *
     * @return string
     */
    public function getFascia()
    {
        return $this->fascia;
    }

    /**
     * Set giornoApertura
     *
     * @param \AppBundle\Entity\GiornoApertura $giornoApertura
     *
     * @return Turno
     */
    public function setGiornoApertura(\AppBundle\Entity\GiornoApertura $giornoApertura)
    {
        $this->giornoApertura = $giornoApertura;

        return $this;
    }

    /**
     * Get giornoApertura
     *
     * @return \AppBundle\Entity\GiornoApertura
     */
    public function getGiornoApertura()
    {
        return $this->giornoApertura;
    }

    /**
     * Set dipendente
     *
     * @param \AppBundle\Entity\Dipendente $dipendente
     *
     * @return Turno
     */
    public function setDipendente(\AppBundle\Entity\Dipendente $dipendente)
    {
        $this->dipendente = $dipendente;

        return $this;
    }

    /**
     * Get dipendente
     *
     * @return \AppBundle\Entity\Dipendente
     */
    public function getDipendente()
    {
        return $this->dipendente;
    }

    /**
     * Get codTurno
     *
     * @return integer
     */
    public function getCodTurno()
    {
        return $this->codTurno;
    }

    /**
     * Set oraInizio
     *
     * @param \DateTime $oraInizio
     *
     * @return Turno
     */
    public function setOraInizio($oraInizio)
    {
        $this->oraInizio = $oraInizio;

        return $this;
    }

    /**
     * Get oraInizio
     *
     * @return \DateTime
     */
    public function getOraInizio()
    {
        return $this->oraInizio;
    }
}
