<?php

namespace AppBundle\Entity;

use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Doctrine\ORM\Mapping as ORM;

/**
 * Dipendente
 *
 * @ORM\Table(name="dipendente")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DipendenteRepository")
 * @ExclusionPolicy("all")
 */
class Dipendente
{
    /**
     * @var int
     *
     * @ORM\Column(name="codDipendente", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $codDipendente;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=50)
     * @Expose
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(name="cognome", type="string", length=50)
     * @Expose
     */
    private $cognome;

    /**
     * @var string
     *
     * @ORM\Column(name="telefono", type="string", length=50, unique=true)
     */
    private $telefono;

    /**
     * @var string
     *
     * @ORM\Column(name="indirizzo", type="string", length=100)
     */
    private $indirizzo;

    /**
     * @var string
     *
     * @ORM\Column(name="citta", type="string", length=100)
     */
    private $citta;

    /**
     * @var string
     *
     * @ORM\Column(name="codiceFiscale", type="string", length=16, unique=true)
     */
    private $codiceFiscale;

    /**
     * @var string
     *
     * @ORM\Column(name="ruolo", type="string", length=50)
     * @Expose
     */
    private $ruolo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dataAssunzione", type="datetime")
     */
    private $dataAssunzione;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dataFineContratto", type="datetime", nullable=true)
     */
    private $dataFineContratto;

    /**
     * @var int
     *
     * @ORM\Column(name="stipendio", type="integer")
     */
    private $stipendio;

    /**
     * @ORM\ManyToOne(targetEntity="Ristorante", inversedBy="dipendenti")
     * @ORM\JoinColumn(name="codRistorante", referencedColumnName="id", nullable=FALSE)
     */
    protected $ristorante;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Turno", mappedBy="dipendente")
     **/
    private $turni;


    /**
     * Get codDipendente
     *
     * @return int
     */
    public function getCodDipendente()
    {
        return $this->codDipendente;
    }

    /**
     * Set codiceFiscale
     *
     * @param string $codiceFiscale
     *
     * @return Dipendente
     */
    public function setCodiceFiscale($codiceFiscale)
    {
        $this->codiceFiscale = $codiceFiscale;

        return $this;
    }

    /**
     * Get codiceFiscale
     *
     * @return string
     */
    public function getCodiceFiscale()
    {
        return $this->codiceFiscale;
    }

    /**
     * Set ruolo
     *
     * @param string $ruolo
     *
     * @return Dipendente
     */
    public function setRuolo($ruolo)
    {
        $this->ruolo = $ruolo;

        return $this;
    }

    /**
     * Get ruolo
     *
     * @return string
     */
    public function getRuolo()
    {
        return $this->ruolo;
    }

    /**
     * Set dataAssunzione
     *
     * @param \DateTime $dataAssunzione
     *
     * @return Dipendente
     */
    public function setDataAssunzione($dataAssunzione)
    {
        $this->dataAssunzione = $dataAssunzione;

        return $this;
    }

    /**
     * Get dataAssunzione
     *
     * @return \DateTime
     */
    public function getDataAssunzione()
    {
        return $this->dataAssunzione;
    }

    /**
     * Set dataFineContratto
     *
     * @param \DateTime $dataFineContratto
     *
     * @return Dipendente
     */
    public function setDataFineContratto($dataFineContratto)
    {
        $this->dataFineContratto = $dataFineContratto;

        return $this;
    }

    /**
     * Get dataFineContratto
     *
     * @return \DateTime
     */
    public function getDataFineContratto()
    {
        return $this->dataFineContratto;
    }

    /**
     * Set stipendio
     *
     * @param integer $stipendio
     *
     * @return Dipendente
     */
    public function setStipendio($stipendio)
    {
        $this->stipendio = $stipendio;

        return $this;
    }

    /**
     * Get stipendio
     *
     * @return int
     */
    public function getStipendio()
    {
        return $this->stipendio;
    }

    /**
     * Set ristorante
     *
     * @param \AppBundle\Entity\Ristorante $ristorante
     *
     * @return Dipendente
     */
    public function setRistorante(\AppBundle\Entity\Ristorante $ristorante)
    {
        $this->ristorante = $ristorante;

        return $this;
    }

    /**
     * Get ristorante
     *
     * @return \AppBundle\Entity\Ristorante
     */
    public function getRistorante()
    {
        return $this->ristorante;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->turni = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add turni
     *
     * @param \AppBundle\Entity\Turno $turni
     *
     * @return Dipendente
     */
    public function addTurni(\AppBundle\Entity\Turno $turni)
    {
        $this->turni[] = $turni;

        return $this;
    }

    /**
     * Remove turni
     *
     * @param \AppBundle\Entity\Turno $turni
     */
    public function removeTurni(\AppBundle\Entity\Turno $turni)
    {
        $this->turni->removeElement($turni);
    }

    /**
     * Get turni
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTurni()
    {
        return $this->turni;
    }

    /**
     * Set nome
     *
     * @param string $nome
     *
     * @return Dipendente
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set cognome
     *
     * @param string $cognome
     *
     * @return Dipendente
     */
    public function setCognome($cognome)
    {
        $this->cognome = $cognome;

        return $this;
    }

    /**
     * Get cognome
     *
     * @return string
     */
    public function getCognome()
    {
        return $this->cognome;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     *
     * @return Dipendente
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set indirizzo
     *
     * @param string $indirizzo
     *
     * @return Dipendente
     */
    public function setIndirizzo($indirizzo)
    {
        $this->indirizzo = $indirizzo;

        return $this;
    }

    /**
     * Get indirizzo
     *
     * @return string
     */
    public function getIndirizzo()
    {
        return $this->indirizzo;
    }

    /**
     * Set citta
     *
     * @param string $citta
     *
     * @return Dipendente
     */
    public function setCitta($citta)
    {
        $this->citta = $citta;

        return $this;
    }

    /**
     * Get citta
     *
     * @return string
     */
    public function getCitta()
    {
        return $this->citta;
    }
}
