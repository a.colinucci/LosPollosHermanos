-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Lug 13, 2017 alle 02:33
-- Versione del server: 5.7.14
-- Versione PHP: 7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lospolloshermanos`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `cliente`
--

CREATE TABLE `cliente` (
  `id` int(11) NOT NULL,
  `nome` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `cognome` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `cellulare` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `indirizzo` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `citta` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `numeroCarta` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `meseScadenzaCarta` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `annoScadenzaCarta` int(11) NOT NULL,
  `cvvCarta` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `cliente`
--

INSERT INTO `cliente` (`id`, `nome`, `cognome`, `cellulare`, `indirizzo`, `citta`, `numeroCarta`, `meseScadenzaCarta`, `annoScadenzaCarta`, `cvvCarta`) VALUES
(1, 'Edoardo', 'Casanova', '333 4256799', 'Via Dei Pini, 21', 'Ancona', '1234567890123456', '5', 2022, 123);

-- --------------------------------------------------------

--
-- Struttura della tabella `dipendente`
--

CREATE TABLE `dipendente` (
  `codDipendente` int(11) NOT NULL,
  `nome` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `cognome` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `telefono` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `indirizzo` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `citta` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `codiceFiscale` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `ruolo` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `dataAssunzione` datetime NOT NULL,
  `dataFineContratto` datetime DEFAULT NULL,
  `stipendio` int(11) NOT NULL,
  `codRistorante` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `dipendente`
--

INSERT INTO `dipendente` (`codDipendente`, `nome`, `cognome`, `telefono`, `indirizzo`, `citta`, `codiceFiscale`, `ruolo`, `dataAssunzione`, `dataFineContratto`, `stipendio`, `codRistorante`) VALUES
(1, 'Mario', 'Rossi', '331556614', 'Via Cavour 54', 'Milano', 'MRIRSS1DW41ODA45', 'Cuoco', '2013-06-18 00:00:00', NULL, 1600, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `fornitore`
--

CREATE TABLE `fornitore` (
  `id` int(11) NOT NULL,
  `nomeAzienda` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `indirizzo` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `citta` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `cellulare` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `partitaIVA` varchar(11) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `fornitore`
--

INSERT INTO `fornitore` (`id`, `nomeAzienda`, `indirizzo`, `citta`, `cellulare`, `partitaIVA`) VALUES
(1, 'Fornitore1 S.r.l.', 'Via Dei Pini, 21', 'Modena', '332 1416789', '14325634748'),
(2, 'Fornitore2 S.r.l.', 'Via Dei Pini, 21', 'Bitonto', '334 5561289', '08978657463'),
(3, 'Fornitore3 S.r.l.', 'Via Dei Pini, 21', 'Perugia', '345 1367901', '45678909876');

-- --------------------------------------------------------

--
-- Struttura della tabella `fornitore_ristorante`
--

CREATE TABLE `fornitore_ristorante` (
  `fornitore_id` int(11) NOT NULL,
  `ristorante_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `fornitore_ristorante`
--

INSERT INTO `fornitore_ristorante` (`fornitore_id`, `ristorante_id`) VALUES
(1, 1),
(1, 5),
(1, 7),
(1, 8),
(1, 9),
(2, 2),
(2, 3),
(2, 4),
(2, 6),
(2, 8),
(3, 1),
(3, 2),
(3, 4),
(3, 7),
(3, 8),
(3, 9);

-- --------------------------------------------------------

--
-- Struttura della tabella `fornitura`
--

CREATE TABLE `fornitura` (
  `id` int(11) NOT NULL,
  `ristorante_id` int(11) NOT NULL,
  `fornitore_id` int(11) NOT NULL,
  `prodotto_id` int(11) NOT NULL,
  `dataRichiesta` datetime NOT NULL,
  `prezzo` double NOT NULL,
  `quantita` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struttura della tabella `fos_user`
--

CREATE TABLE `fos_user` (
  `id` int(11) NOT NULL,
  `fornitore_id` int(11) DEFAULT NULL,
  `cliente_id` int(11) DEFAULT NULL,
  `username` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `fos_user`
--

INSERT INTO `fos_user` (`id`, `fornitore_id`, `cliente_id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `confirmation_token`, `password_requested_at`, `roles`) VALUES
(1, NULL, 1, 'edoardo.casanova', 'edoardo.casanova', 'edoardo.casanova@gmail.com', 'edoardo.casanova@gmail.com', 1, NULL, '$2y$13$Bwj5kOSjFr2ZIs8RWfaoRuS4nuN9/cB7f0YuIjQa36sPhENq.RkRS', '2017-07-12 23:56:46', NULL, NULL, 'a:0:{}'),
(2, 1, NULL, 'fornitore1', 'fornitore1', 'fornitore@gmail.com', 'fornitore@gmail.com', 1, NULL, '$2y$13$1v7ORwBB9S3O0/rjlMarWegWFvJG/MyuyEMW3TIXachisONgqjDwu', '2017-07-12 23:24:08', NULL, NULL, 'a:1:{i:0;s:14:"ROLE_FORNITORE";}'),
(3, 2, NULL, 'fornitore2', 'fornitore2', 'fornitore2@gmail.com', 'fornitore2@gmail.com', 1, NULL, '$2y$13$WfKiCNVM.BfBbYqnbLZzmuj6bf1U6qH4fVOC1S2txydMp2y7BZGhO', '2017-07-11 17:47:58', NULL, NULL, 'a:1:{i:0;s:14:"ROLE_FORNITORE";}'),
(4, 3, NULL, 'fornitore3', 'fornitore3', 'fornitore3@gmail.com', 'fornitore3@gmail.com', 1, NULL, '$2y$13$8BG8BMisGeo8EyLif.DNIuw2LBpHxPKqSWfjE5FCPH3KIw7bWi1XW', '2017-07-11 17:43:38', NULL, NULL, 'a:1:{i:0;s:14:"ROLE_FORNITORE";}');

-- --------------------------------------------------------

--
-- Struttura della tabella `giorno_apertura`
--

CREATE TABLE `giorno_apertura` (
  `giorno` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ristorante_id` int(11) NOT NULL,
  `orarioAperturaPranzo` time DEFAULT NULL,
  `orarioChiusuraPranzo` time DEFAULT NULL,
  `orarioAperturaCena` time DEFAULT NULL,
  `orarioChiusuraCena` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `giorno_apertura`
--

INSERT INTO `giorno_apertura` (`giorno`, `ristorante_id`, `orarioAperturaPranzo`, `orarioChiusuraPranzo`, `orarioAperturaCena`, `orarioChiusuraCena`) VALUES
('13/07/2017', 1, '12:00:00', '15:00:00', '19:00:00', '23:30:00'),
('14/07/2017', 1, NULL, NULL, '18:30:00', '23:30:00'),
('15/07/2017', 1, '12:30:00', '15:30:00', '19:00:00', '23:30:00'),
('16/07/2017', 1, '12:00:00', '16:00:00', '18:30:00', '23:00:00'),
('17/07/2017', 1, '12:30:00', '15:30:00', '19:00:00', '23:30:00');

-- --------------------------------------------------------

--
-- Struttura della tabella `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `ristorante_id` int(11) DEFAULT NULL,
  `descrizione` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `menu`
--

INSERT INTO `menu` (`id`, `ristorante_id`, `descrizione`) VALUES
(1, 1, 'Questo è il menu di LosPollosHermanos di Milano ');

-- --------------------------------------------------------

--
-- Struttura della tabella `ordine`
--

CREATE TABLE `ordine` (
  `cap` int(11) NOT NULL,
  `cliente_id` int(11) DEFAULT NULL,
  `codOrdine` int(11) NOT NULL,
  `dataEffettuazione` datetime NOT NULL,
  `oraConsegna` time NOT NULL,
  `cittaConsegna` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `indirizzoConsegna` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prezzoTotale` double NOT NULL,
  `consegnato` tinyint(1) NOT NULL,
  `codRistorante` int(11) DEFAULT NULL,
  `codRecensione` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `ordine`
--

INSERT INTO `ordine` (`cap`, `cliente_id`, `codOrdine`, `dataEffettuazione`, `oraConsegna`, `cittaConsegna`, `indirizzoConsegna`, `prezzoTotale`, `consegnato`, `codRistorante`, `codRecensione`) VALUES
(41345, 1, 1, '2017-07-13 00:53:47', '21:00:00', 'Sesto San Giovanni', 'Via Cavour 54', 157, 1, 1, NULL);

-- --------------------------------------------------------

--
-- Struttura della tabella `pietanza`
--

CREATE TABLE `pietanza` (
  `sezione_id` int(11) DEFAULT NULL,
  `codPietanza` int(11) NOT NULL,
  `nome` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `calorie` int(11) NOT NULL,
  `descrizione` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prezzo` double NOT NULL,
  `grassi` double DEFAULT NULL,
  `carboidrati` double DEFAULT NULL,
  `zuccheri` double DEFAULT NULL,
  `proteine` double DEFAULT NULL,
  `sale` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `pietanza`
--

INSERT INTO `pietanza` (`sezione_id`, `codPietanza`, `nome`, `calorie`, `descrizione`, `prezzo`, `grassi`, `carboidrati`, `zuccheri`, `proteine`, `sale`) VALUES
(1, 2, 'Mozzarelle Stick', 320, '6 pezzi di mozzarella di bufala impanati e fritti a bastoncino', 4, 3.4, 3.2, 2.12, 1.4, 3.6),
(1, 4, 'Jalapeños', 420, '6 jalapeños ripieni e fritti', 5, 4.3, 3.5, 2.3, 1.9, 4.1),
(6, 5, 'Acqua Naturale 0.5L', 10, '', 1, 0.3, 0.2, 0.15, 0.08, 0.12),
(6, 6, 'Acqua Frizzante 0.5L', 10, '', 1, 0.3, 0.2, 0.2, 0.05, 0.15),
(6, 7, 'Acqua Naturale 1L', 17, '', 2, 0.5, 0.3, 0.2, 0.15, 0.2),
(6, 8, 'Acqua Frizzante 1L', 20, '', 2, 0.5, 0.3, 0.2, 0.15, 0.2),
(6, 9, 'Weiss piccola', 80, 'Birra "Weiss" alla spina - 0.3L', 3, 0.34, 0.29, 0.38, 0.49, 2.3),
(6, 10, 'Weiss media', 120, 'Birra "Weiss" alla spina - 0.5L', 5, 0.4, 0.6, 1.2, 0.2, 2.1),
(6, 11, 'Weiss grande', 150, 'Birra "Weiss" alla spina nel boccale da 1L', 8, 1.2, 1.3, 2.5, 0.8, 2.1),
(2, 12, 'Caesar Salad', 180, 'Insalata di pollo con salsa Caesar', 7, 1.8, 2.3, 1.2, 1.8, 2.4),
(1, 13, 'Patatine fritte', 220, 'Patate fritte alla contadina', 3.5, 3.1, 2.8, 0.8, 0.4, 3.2),
(4, 14, 'Costolette di maiale', 320, '6 costolette di maiale marinate in salsa BBQ', 8, 3.2, 2.8, 1.3, 1.4, 3.8),
(2, 15, 'Insalata Greca', 90, 'Un\' insalata fresca, ottima per l\'estate', 7, 1.2, 1.4, 0.8, 1.7, 1.9),
(3, 16, 'CheeseBurger', 310, 'Un classico degli hambuger con formaggio Cheddar', 7.5, 3.4, 3.6, 2.1, 1.6, 3.7),
(3, 17, 'BaconBurger', 350, 'L\' hamburger più tradizionale con bacon croccante', 7.5, 3.5, 3.2, 2.1, 1.8, 3.9),
(3, 18, 'Boscaiolo', 280, 'Un hamburger creato appositamente per chi ama sapori di montagna', 8.5, 3.2, 4.1, 1.8, 2.9, 3.4),
(4, 19, 'Fiorentina ', 250, 'Un classico. La nostra Fiorentina da 700g', 17, 3.7, 4.1, 2.5, 2.3, 3.25),
(5, 20, 'Tortino al Cioccolato', 360, 'Tortino con cuore fondente. Una tentazione a cui non si può resistere.', 5, 3.8, 3.1, 4.3, 1.85, 2.1);

-- --------------------------------------------------------

--
-- Struttura della tabella `pietanza_in_ordine`
--

CREATE TABLE `pietanza_in_ordine` (
  `quantita` int(11) NOT NULL,
  `codOrdine` int(11) NOT NULL,
  `codPietanza` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `pietanza_in_ordine`
--

INSERT INTO `pietanza_in_ordine` (`quantita`, `codOrdine`, `codPietanza`) VALUES
(3, 1, 2),
(3, 1, 4),
(2, 1, 12),
(3, 1, 13),
(1, 1, 14),
(3, 1, 15),
(2, 1, 17),
(6, 1, 18),
(2, 1, 20);

-- --------------------------------------------------------

--
-- Struttura della tabella `pietanze_ingredienti`
--

CREATE TABLE `pietanze_ingredienti` (
  `codPietanza` int(11) NOT NULL,
  `codProdotto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `pietanze_ingredienti`
--

INSERT INTO `pietanze_ingredienti` (`codPietanza`, `codProdotto`) VALUES
(2, 18),
(2, 77),
(2, 82),
(4, 18),
(4, 22),
(4, 78),
(4, 79),
(4, 81),
(4, 82),
(5, 37),
(6, 38),
(7, 37),
(8, 38),
(9, 41),
(10, 41),
(11, 41),
(12, 5),
(12, 6),
(12, 11),
(12, 16),
(12, 24),
(12, 29),
(13, 1),
(13, 18),
(14, 9),
(14, 10),
(15, 2),
(15, 4),
(15, 11),
(15, 13),
(15, 16),
(15, 83),
(15, 84),
(16, 2),
(16, 5),
(16, 11),
(16, 13),
(16, 21),
(16, 22),
(17, 2),
(17, 5),
(17, 11),
(17, 13),
(17, 21),
(17, 86),
(18, 5),
(18, 9),
(18, 11),
(18, 13),
(18, 21),
(18, 26),
(18, 27),
(18, 86),
(19, 20),
(19, 24),
(19, 85),
(20, 3);

-- --------------------------------------------------------

--
-- Struttura della tabella `prenotazione`
--

CREATE TABLE `prenotazione` (
  `codPrenotazione` int(11) NOT NULL,
  `persone` int(11) NOT NULL,
  `data` datetime NOT NULL,
  `orario` time NOT NULL,
  `numeroTavolo` int(11) NOT NULL,
  `codRistorante` int(11) NOT NULL,
  `codCliente` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `prenotazione`
--

INSERT INTO `prenotazione` (`codPrenotazione`, `persone`, `data`, `orario`, `numeroTavolo`, `codRistorante`, `codCliente`) VALUES
(1, 4, '2017-07-13 21:00:00', '21:00:00', 1, 1, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `prodotto`
--

CREATE TABLE `prodotto` (
  `id` int(11) NOT NULL,
  `nome` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `descrizione` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tipo` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `prodotto`
--

INSERT INTO `prodotto` (`id`, `nome`, `descrizione`, `tipo`) VALUES
(1, 'Patata', '', 'Ingrediente'),
(2, 'Cipolla rossa', '', 'Ingrediente'),
(3, 'Cioccolato', 'Cioccolato fondente 70%', 'Ingrediente'),
(4, 'Pane di segale', '', 'Ingrediente'),
(5, 'Pane integrale', '', 'Ingrediente'),
(6, 'Pollo', 'Pollo intero', 'Ingrediente'),
(7, 'Ketchup', '', 'Ingrediente'),
(8, 'Maionese', '', 'Ingrediente'),
(9, 'Salsa BBQ', 'Salsa barbeque agrodolce', 'Ingrediente'),
(10, 'Costolette', 'Costolette di maiale', 'Ingrediente'),
(11, 'Lattuga', '', 'Ingrediente'),
(12, 'Radicchio', '', 'Ingrediente'),
(13, 'Pomodoro', '', 'Ingrediente'),
(14, 'Zucchina', '', 'Ingrediente'),
(15, 'Melanzana', '', 'Ingrediente'),
(16, 'Olio EVO', 'Olio extravergine d\'oliva', 'Ingrediente'),
(17, 'Olio di arachidi', '', 'Ingrediente'),
(18, 'Olio di semi di girasole', 'Olio per fritture', 'Ingrediente'),
(19, 'Aceto di vino', 'Aceto di vino bianco', 'Ingrediente'),
(20, 'Aceto balsamico', 'Aceto balsamico di Modena Ponti', 'Ingrediente'),
(21, 'Svizzera di manzo', 'Svizzera 200g', 'Ingrediente'),
(22, 'Formaggio Cheddar', '', 'Ingrediente'),
(23, 'Pecorino', 'Formaggio pecorino', 'Ingrediente'),
(24, 'Parmigiano reggiano', 'Formaggio parmigiano reggiano', 'Ingrediente'),
(26, 'Formaggio di fossa', '', 'Ingrediente'),
(27, 'Scamorza', '', 'Ingrediente'),
(28, 'Salsa Cocktail', '', 'Ingrediente'),
(29, 'Salsa Ceasar', 'Salsa di formaggio per insalate', 'Ingrediente'),
(30, 'Uva', '', 'Ingrediente'),
(31, 'Arachidi', 'Frutta secca', 'Ingrediente'),
(32, 'Anacardi', 'Frutta secca', 'Ingrediente'),
(33, 'Mandorla', 'Frutta secca', 'Ingrediente'),
(34, 'Noce', 'Frutta secca', 'Ingrediente'),
(37, 'Acqua Naturale', '', 'Ingrediente'),
(38, 'Acqua Frizzante', '', 'Ingrediente'),
(39, 'Vino Bianco', '', 'Ingrediente'),
(40, 'Vino Rosso', '', 'Ingrediente'),
(41, 'Birra', '', 'Ingrediente'),
(42, 'Forno', '', 'Strumento da cucina'),
(43, 'Forno a microonde', '', 'Strumento da cucina'),
(44, 'Pentola 25cm', 'Pentola 25cm diametro', 'Strumento da cucina'),
(45, 'Pentola 20cm', 'Pentola 20cm diametro', 'Strumento da cucina'),
(46, 'Friggitrice', '', 'Strumento da cucina'),
(47, 'Mestolo', '', 'Strumento da cucina'),
(48, 'Guanti da forno', '', 'Strumento da cucina'),
(49, 'Carta forno', '', 'Strumento da cucina'),
(50, 'Pellicola trasparente', '', 'Strumento da cucina'),
(51, 'Carta stagnola', '', 'Strumento da cucina'),
(52, 'Tagliere di legno', '', 'Strumento da cucina'),
(53, 'Coltello da pane', 'Lunghezza 20cm', 'Strumento da cucina'),
(54, 'Coltello da affilettatura', '', 'Strumento da cucina'),
(55, 'Grembiule', 'Colore: bianco', 'Strumento da cucina'),
(56, 'Pentolone', 'Pentolone diametro 40cm', 'Strumento da cucina'),
(57, 'Congelatore', 'Volume 400L', 'Strumento da cucina'),
(58, 'Tovaglia ', '', 'Oggetto per il servizio'),
(59, 'Forchetta', '', 'Oggetto per il servizio'),
(60, 'Coltello', '', 'Oggetto per il servizio'),
(61, 'Cucchiaio', '', 'Oggetto per il servizio'),
(62, 'Calice vino bianco', 'Calice stretto', 'Oggetto per il servizio'),
(63, 'Calice vino rosso', 'Calice largo', 'Oggetto per il servizio'),
(64, 'Bicchiere birra 0.3L', 'Bicchiere per birra piccola', 'Oggetto per il servizio'),
(65, 'Bicchiere birra 0.5L', 'Bicchiere per birra media', 'Oggetto per il servizio'),
(66, 'Boccale birra 1L', 'Boccale per birra da litro', 'Oggetto per il servizio'),
(67, 'Stuzzicadente', 'Stuzzicadente Samurai', 'Oggetto per il servizio'),
(68, 'Cucchiaino', 'Cucchiaino da dolce', 'Oggetto per il servizio'),
(69, 'Tovagliolo', '', 'Oggetto per il servizio'),
(71, 'Centrotavola', '', 'Oggetto per il servizio'),
(72, 'Tovaglietta', 'Tovaglietta di carta personalizzata LosPollosHermanos', 'Oggetto per il servizio'),
(73, 'Bicchiere per l\'acqua', 'Bicchiere tumbler', 'Oggetto per il servizio'),
(74, 'Piatto fondo', '', 'Oggetto per il servizio'),
(75, 'Piatto piano', '', 'Oggetto per il servizio'),
(76, 'Piattino ', 'Piattino da dolce', 'Oggetto per il servizio'),
(77, 'Mozzarella di bufala', 'Provenienza campana', 'Ingrediente'),
(78, 'Peperone', 'Peperone classico (rosso, verde, giallo)', 'Ingrediente'),
(79, 'Peperoncino', 'Peperoncino piccante', 'Ingrediente'),
(80, 'Olio Tartufato', '', 'Ingrediente'),
(81, 'Salsa piccante', 'Salsa a base di peperoncino piccante', 'Ingrediente'),
(82, 'Pan grattato', 'Pane grattugiato da usare per fritture', 'Ingrediente'),
(83, 'Feta', 'Formaggio', 'Ingrediente'),
(84, 'Olive taggiasche', '', 'Ingrediente'),
(85, 'Fiorentina', '1Kg di fiorentina', 'Ingrediente'),
(86, 'Pancetta', '', 'Ingrediente');

-- --------------------------------------------------------

--
-- Struttura della tabella `prodotto_in_listino`
--

CREATE TABLE `prodotto_in_listino` (
  `fornitore_id` int(11) NOT NULL,
  `prodotto_id` int(11) NOT NULL,
  `prezzo` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `prodotto_in_listino`
--

INSERT INTO `prodotto_in_listino` (`fornitore_id`, `prodotto_id`, `prezzo`) VALUES
(1, 7, 0.05),
(1, 8, 0.05),
(1, 9, 0.05),
(1, 16, 2),
(1, 17, 1.8),
(1, 18, 1.5),
(1, 19, 1.2),
(1, 20, 2.3),
(1, 28, 0.1),
(1, 29, 0.1),
(1, 42, 2000),
(1, 43, 500),
(1, 44, 5),
(1, 45, 3.5),
(1, 46, 400),
(1, 47, 0.5),
(1, 48, 1),
(1, 49, 0.2),
(1, 50, 0.15),
(1, 52, 15),
(1, 53, 12),
(1, 54, 25),
(1, 56, 14),
(1, 57, 800),
(2, 1, 0.2),
(2, 2, 0.2),
(2, 3, 0.8),
(2, 4, 0.25),
(2, 5, 0.25),
(2, 6, 4),
(2, 7, 0.15),
(2, 8, 0.15),
(2, 9, 0.15),
(2, 10, 0.15),
(2, 11, 0.1),
(2, 12, 0.1),
(2, 13, 0.15),
(2, 14, 0.35),
(2, 15, 0.3),
(2, 21, 1.5),
(2, 22, 2.5),
(2, 23, 6.4),
(2, 24, 3.2),
(2, 26, 8.2),
(2, 27, 3.5),
(2, 58, 5),
(2, 59, 0.3),
(2, 60, 0.3),
(2, 61, 0.3),
(2, 68, 0.3),
(2, 69, 0.1),
(2, 71, 0.25),
(2, 72, 0.2),
(2, 74, 1.3),
(2, 75, 1.5),
(2, 76, 1.2),
(3, 1, 0.15),
(3, 6, 3.5),
(3, 9, 0.1),
(3, 10, 3.2),
(3, 11, 0.15),
(3, 12, 0.15),
(3, 13, 0.22),
(3, 16, 6.3),
(3, 17, 2.5),
(3, 20, 8.1),
(3, 30, 0.6),
(3, 37, 0.25),
(3, 38, 0.25),
(3, 39, 3.5),
(3, 40, 4.5),
(3, 41, 2.5),
(3, 62, 0.35),
(3, 63, 0.35),
(3, 64, 0.45),
(3, 65, 0.9),
(3, 66, 1.5),
(3, 67, 0.05),
(3, 73, 0.4);

-- --------------------------------------------------------

--
-- Struttura della tabella `prodotto_in_magazzino`
--

CREATE TABLE `prodotto_in_magazzino` (
  `quantita` int(11) NOT NULL,
  `codRistorante` int(11) NOT NULL,
  `codProdotto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `prodotto_in_magazzino`
--

INSERT INTO `prodotto_in_magazzino` (`quantita`, `codRistorante`, `codProdotto`) VALUES
(150, 1, 1),
(20, 1, 2),
(10, 1, 3),
(30, 1, 4),
(50, 1, 5),
(5, 1, 16),
(15, 1, 20),
(40, 1, 21),
(7, 1, 26),
(80, 1, 37),
(50, 1, 38),
(30, 1, 41),
(3, 1, 85),
(5, 1, 86);

-- --------------------------------------------------------

--
-- Struttura della tabella `recensione`
--

CREATE TABLE `recensione` (
  `ristorante_id` int(11) DEFAULT NULL,
  `codRecensione` int(11) NOT NULL,
  `voto` int(11) NOT NULL,
  `data` datetime NOT NULL,
  `titolo` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `testo` varchar(2000) COLLATE utf8_unicode_ci NOT NULL,
  `codOrdine` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struttura della tabella `ristorante`
--

CREATE TABLE `ristorante` (
  `id` int(11) NOT NULL,
  `nome` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `maxCoperti` int(11) DEFAULT NULL,
  `indirizzo` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `citta` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `telefono` varchar(30) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `ristorante`
--

INSERT INTO `ristorante` (`id`, `nome`, `maxCoperti`, `indirizzo`, `citta`, `telefono`) VALUES
(1, 'Los Pollos Hermanos - Milano', 37, 'Via Emilio Zola, 52', 'Milano', '0548 456911'),
(2, 'Los Pollos Hermanos - Roma', 0, 'Viale Muliére, 12', 'Roma', '0544 146921'),
(3, 'Los Pollos Hermanos - Bari', 0, 'Via Michele Cifarelli, 114', 'Bari', '0542 851356'),
(4, 'Los Pollos Hermanos - Napoli', 0, 'Via Ludovico Bianchini, 10', 'Napoli', '0546 678913'),
(5, 'Los Pollos Hermanos - Torino', 0, 'Corso Brescia, 22', 'Torino', '0541 890155'),
(6, 'Los Pollos Hermanos - Palermo', 0, 'Via Salvatore Aldisio, 43', 'Palermo', '0545 896411'),
(7, 'Los Pollos Hermanos - Firenze', 0, 'Via Villani, 89', 'Firenze', '0546 892145'),
(8, 'Los Pollos Hermanos - Bologna', 0, 'Via Mario Musolesi, 95', 'Bologna', '0544 234561'),
(9, 'Los Pollos Hermanos - Verona', 0, 'Via Armando Diaz, 58', 'Verona', '0545 456791');

-- --------------------------------------------------------

--
-- Struttura della tabella `sezione`
--

CREATE TABLE `sezione` (
  `id` int(11) NOT NULL,
  `menu_id` int(11) DEFAULT NULL,
  `nome` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `descrizione` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `sezione`
--

INSERT INTO `sezione` (`id`, `menu_id`, `nome`, `descrizione`) VALUES
(1, 1, 'Antipasti', 'Giusto per iniziare'),
(2, 1, 'Insalate', 'Le nostre insalate farcite con pollo '),
(3, 1, 'Hamburgers', 'Una selezione di hamburger incredibile!'),
(4, 1, 'Secondi', 'Qualità e sostanza!'),
(5, 1, 'Dolci', 'Per concludere in bellezza'),
(6, 1, 'Bevande', 'Birre formidabili');

-- --------------------------------------------------------

--
-- Struttura della tabella `tariffario`
--

CREATE TABLE `tariffario` (
  `cap` int(11) NOT NULL,
  `prezzo` int(11) NOT NULL,
  `codRistorante` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `tariffario`
--

INSERT INTO `tariffario` (`cap`, `prezzo`, `codRistorante`) VALUES
(41345, 6, 1),
(41356, 5, 1),
(41391, 4, 1),
(47395, 3, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `tavolo`
--

CREATE TABLE `tavolo` (
  `numero` int(11) NOT NULL,
  `ristorante_id` int(11) NOT NULL,
  `posti` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `tavolo`
--

INSERT INTO `tavolo` (`numero`, `ristorante_id`, `posti`) VALUES
(1, 1, 4),
(2, 1, 2),
(3, 1, 8),
(4, 1, 6),
(5, 1, 3),
(6, 1, 12),
(7, 1, 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `turno`
--

CREATE TABLE `turno` (
  `giorno` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `codTurno` int(11) NOT NULL,
  `fascia` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `oraInizio` time NOT NULL,
  `oraFine` time NOT NULL,
  `codRistorante` int(11) NOT NULL,
  `codDipendente` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_F41C9B25746D1552` (`numeroCarta`);

--
-- Indici per le tabelle `dipendente`
--
ALTER TABLE `dipendente`
  ADD PRIMARY KEY (`codDipendente`),
  ADD UNIQUE KEY `UNIQ_D8EE7EC8C1E70A7F` (`telefono`),
  ADD UNIQUE KEY `UNIQ_D8EE7EC8F4D5169D` (`codiceFiscale`),
  ADD KEY `IDX_D8EE7EC8BCB7B459` (`codRistorante`);

--
-- Indici per le tabelle `fornitore`
--
ALTER TABLE `fornitore`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_9A97D1F69ACBAFBC` (`partitaIVA`);

--
-- Indici per le tabelle `fornitore_ristorante`
--
ALTER TABLE `fornitore_ristorante`
  ADD PRIMARY KEY (`fornitore_id`,`ristorante_id`),
  ADD KEY `IDX_B4BC07C021403871` (`fornitore_id`),
  ADD KEY `IDX_B4BC07C0137F643B` (`ristorante_id`);

--
-- Indici per le tabelle `fornitura`
--
ALTER TABLE `fornitura`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_8C4B3349137F643B` (`ristorante_id`),
  ADD KEY `IDX_8C4B334921403871` (`fornitore_id`),
  ADD KEY `IDX_8C4B3349AB38982D` (`prodotto_id`);

--
-- Indici per le tabelle `fos_user`
--
ALTER TABLE `fos_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_957A647992FC23A8` (`username_canonical`),
  ADD UNIQUE KEY `UNIQ_957A6479A0D96FBF` (`email_canonical`),
  ADD UNIQUE KEY `UNIQ_957A6479C05FB297` (`confirmation_token`),
  ADD UNIQUE KEY `UNIQ_957A647921403871` (`fornitore_id`),
  ADD UNIQUE KEY `UNIQ_957A6479DE734E51` (`cliente_id`);

--
-- Indici per le tabelle `giorno_apertura`
--
ALTER TABLE `giorno_apertura`
  ADD PRIMARY KEY (`giorno`,`ristorante_id`),
  ADD KEY `IDX_F8E9CF91137F643B` (`ristorante_id`);

--
-- Indici per le tabelle `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_7D053A93137F643B` (`ristorante_id`);

--
-- Indici per le tabelle `ordine`
--
ALTER TABLE `ordine`
  ADD PRIMARY KEY (`codOrdine`),
  ADD UNIQUE KEY `UNIQ_FE96EC8665F6A30A` (`codRecensione`),
  ADD KEY `IDX_FE96EC86BCB7B459` (`codRistorante`),
  ADD KEY `IDX_FE96EC86993387B1BCB7B459` (`cap`,`codRistorante`),
  ADD KEY `IDX_FE96EC86DE734E51` (`cliente_id`);

--
-- Indici per le tabelle `pietanza`
--
ALTER TABLE `pietanza`
  ADD PRIMARY KEY (`codPietanza`),
  ADD KEY `IDX_AD8129D781E679D8` (`sezione_id`);

--
-- Indici per le tabelle `pietanza_in_ordine`
--
ALTER TABLE `pietanza_in_ordine`
  ADD PRIMARY KEY (`codOrdine`,`codPietanza`),
  ADD KEY `IDX_2E8F739CBD2D3B3D` (`codOrdine`),
  ADD KEY `IDX_2E8F739CB2075A68` (`codPietanza`);

--
-- Indici per le tabelle `pietanze_ingredienti`
--
ALTER TABLE `pietanze_ingredienti`
  ADD PRIMARY KEY (`codPietanza`,`codProdotto`),
  ADD KEY `IDX_E1DF5AE8B2075A68` (`codPietanza`),
  ADD KEY `IDX_E1DF5AE89EF077A4` (`codProdotto`);

--
-- Indici per le tabelle `prenotazione`
--
ALTER TABLE `prenotazione`
  ADD PRIMARY KEY (`codPrenotazione`),
  ADD KEY `IDX_F89BBC7FBE725921BCB7B459` (`numeroTavolo`,`codRistorante`),
  ADD KEY `IDX_F89BBC7FA8EC4AF6` (`codCliente`);

--
-- Indici per le tabelle `prodotto`
--
ALTER TABLE `prodotto`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8176041B54BD530C` (`nome`);

--
-- Indici per le tabelle `prodotto_in_listino`
--
ALTER TABLE `prodotto_in_listino`
  ADD PRIMARY KEY (`fornitore_id`,`prodotto_id`),
  ADD KEY `IDX_7537CF9E21403871` (`fornitore_id`),
  ADD KEY `IDX_7537CF9EAB38982D` (`prodotto_id`);

--
-- Indici per le tabelle `prodotto_in_magazzino`
--
ALTER TABLE `prodotto_in_magazzino`
  ADD PRIMARY KEY (`codRistorante`,`codProdotto`),
  ADD KEY `IDX_7B1150B6BCB7B459` (`codRistorante`),
  ADD KEY `IDX_7B1150B69EF077A4` (`codProdotto`);

--
-- Indici per le tabelle `recensione`
--
ALTER TABLE `recensione`
  ADD PRIMARY KEY (`codRecensione`),
  ADD UNIQUE KEY `UNIQ_CFA72E7DBD2D3B3D` (`codOrdine`),
  ADD KEY `IDX_CFA72E7D137F643B` (`ristorante_id`);

--
-- Indici per le tabelle `ristorante`
--
ALTER TABLE `ristorante`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `sezione`
--
ALTER TABLE `sezione`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_68B49425CCD7E912` (`menu_id`);

--
-- Indici per le tabelle `tariffario`
--
ALTER TABLE `tariffario`
  ADD PRIMARY KEY (`cap`,`codRistorante`),
  ADD KEY `IDX_D269A97DBCB7B459` (`codRistorante`);

--
-- Indici per le tabelle `tavolo`
--
ALTER TABLE `tavolo`
  ADD PRIMARY KEY (`numero`,`ristorante_id`),
  ADD KEY `IDX_9CB99BB6137F643B` (`ristorante_id`);

--
-- Indici per le tabelle `turno`
--
ALTER TABLE `turno`
  ADD PRIMARY KEY (`codTurno`),
  ADD KEY `IDX_E79767622252EAF6BCB7B459` (`giorno`,`codRistorante`),
  ADD KEY `IDX_E797676272BFF3BF` (`codDipendente`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `cliente`
--
ALTER TABLE `cliente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT per la tabella `dipendente`
--
ALTER TABLE `dipendente`
  MODIFY `codDipendente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT per la tabella `fornitore`
--
ALTER TABLE `fornitore`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT per la tabella `fornitura`
--
ALTER TABLE `fornitura`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `fos_user`
--
ALTER TABLE `fos_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT per la tabella `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT per la tabella `ordine`
--
ALTER TABLE `ordine`
  MODIFY `codOrdine` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT per la tabella `pietanza`
--
ALTER TABLE `pietanza`
  MODIFY `codPietanza` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT per la tabella `prenotazione`
--
ALTER TABLE `prenotazione`
  MODIFY `codPrenotazione` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT per la tabella `prodotto`
--
ALTER TABLE `prodotto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;
--
-- AUTO_INCREMENT per la tabella `recensione`
--
ALTER TABLE `recensione`
  MODIFY `codRecensione` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `ristorante`
--
ALTER TABLE `ristorante`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT per la tabella `sezione`
--
ALTER TABLE `sezione`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT per la tabella `turno`
--
ALTER TABLE `turno`
  MODIFY `codTurno` int(11) NOT NULL AUTO_INCREMENT;
--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `dipendente`
--
ALTER TABLE `dipendente`
  ADD CONSTRAINT `FK_D8EE7EC8BCB7B459` FOREIGN KEY (`codRistorante`) REFERENCES `ristorante` (`id`);

--
-- Limiti per la tabella `fornitore_ristorante`
--
ALTER TABLE `fornitore_ristorante`
  ADD CONSTRAINT `FK_B4BC07C0137F643B` FOREIGN KEY (`ristorante_id`) REFERENCES `ristorante` (`id`),
  ADD CONSTRAINT `FK_B4BC07C021403871` FOREIGN KEY (`fornitore_id`) REFERENCES `fornitore` (`id`);

--
-- Limiti per la tabella `fornitura`
--
ALTER TABLE `fornitura`
  ADD CONSTRAINT `FK_8C4B3349137F643B` FOREIGN KEY (`ristorante_id`) REFERENCES `ristorante` (`id`),
  ADD CONSTRAINT `FK_8C4B334921403871` FOREIGN KEY (`fornitore_id`) REFERENCES `fornitore` (`id`),
  ADD CONSTRAINT `FK_8C4B3349AB38982D` FOREIGN KEY (`prodotto_id`) REFERENCES `prodotto` (`id`);

--
-- Limiti per la tabella `fos_user`
--
ALTER TABLE `fos_user`
  ADD CONSTRAINT `FK_957A647921403871` FOREIGN KEY (`fornitore_id`) REFERENCES `fornitore` (`id`),
  ADD CONSTRAINT `FK_957A6479DE734E51` FOREIGN KEY (`cliente_id`) REFERENCES `cliente` (`id`);

--
-- Limiti per la tabella `giorno_apertura`
--
ALTER TABLE `giorno_apertura`
  ADD CONSTRAINT `FK_F8E9CF91137F643B` FOREIGN KEY (`ristorante_id`) REFERENCES `ristorante` (`id`);

--
-- Limiti per la tabella `menu`
--
ALTER TABLE `menu`
  ADD CONSTRAINT `FK_7D053A93137F643B` FOREIGN KEY (`ristorante_id`) REFERENCES `ristorante` (`id`);

--
-- Limiti per la tabella `ordine`
--
ALTER TABLE `ordine`
  ADD CONSTRAINT `FK_FE96EC8665F6A30A` FOREIGN KEY (`codRecensione`) REFERENCES `recensione` (`codRecensione`),
  ADD CONSTRAINT `FK_FE96EC86993387B1BCB7B459` FOREIGN KEY (`cap`,`codRistorante`) REFERENCES `tariffario` (`cap`, `codRistorante`),
  ADD CONSTRAINT `FK_FE96EC86BCB7B459` FOREIGN KEY (`codRistorante`) REFERENCES `ristorante` (`id`),
  ADD CONSTRAINT `FK_FE96EC86DE734E51` FOREIGN KEY (`cliente_id`) REFERENCES `cliente` (`id`);

--
-- Limiti per la tabella `pietanza`
--
ALTER TABLE `pietanza`
  ADD CONSTRAINT `FK_AD8129D781E679D8` FOREIGN KEY (`sezione_id`) REFERENCES `sezione` (`id`);

--
-- Limiti per la tabella `pietanza_in_ordine`
--
ALTER TABLE `pietanza_in_ordine`
  ADD CONSTRAINT `FK_2E8F739CB2075A68` FOREIGN KEY (`codPietanza`) REFERENCES `pietanza` (`codPietanza`),
  ADD CONSTRAINT `FK_2E8F739CBD2D3B3D` FOREIGN KEY (`codOrdine`) REFERENCES `ordine` (`codOrdine`);

--
-- Limiti per la tabella `pietanze_ingredienti`
--
ALTER TABLE `pietanze_ingredienti`
  ADD CONSTRAINT `FK_E1DF5AE89EF077A4` FOREIGN KEY (`codProdotto`) REFERENCES `prodotto` (`id`),
  ADD CONSTRAINT `FK_E1DF5AE8B2075A68` FOREIGN KEY (`codPietanza`) REFERENCES `pietanza` (`codPietanza`);

--
-- Limiti per la tabella `prenotazione`
--
ALTER TABLE `prenotazione`
  ADD CONSTRAINT `FK_F89BBC7FA8EC4AF6` FOREIGN KEY (`codCliente`) REFERENCES `cliente` (`id`),
  ADD CONSTRAINT `FK_F89BBC7FBE725921BCB7B459` FOREIGN KEY (`numeroTavolo`,`codRistorante`) REFERENCES `tavolo` (`numero`, `ristorante_id`);

--
-- Limiti per la tabella `prodotto_in_listino`
--
ALTER TABLE `prodotto_in_listino`
  ADD CONSTRAINT `FK_7537CF9E21403871` FOREIGN KEY (`fornitore_id`) REFERENCES `fornitore` (`id`),
  ADD CONSTRAINT `FK_7537CF9EAB38982D` FOREIGN KEY (`prodotto_id`) REFERENCES `prodotto` (`id`);

--
-- Limiti per la tabella `prodotto_in_magazzino`
--
ALTER TABLE `prodotto_in_magazzino`
  ADD CONSTRAINT `FK_7B1150B69EF077A4` FOREIGN KEY (`codProdotto`) REFERENCES `prodotto` (`id`),
  ADD CONSTRAINT `FK_7B1150B6BCB7B459` FOREIGN KEY (`codRistorante`) REFERENCES `ristorante` (`id`);

--
-- Limiti per la tabella `recensione`
--
ALTER TABLE `recensione`
  ADD CONSTRAINT `FK_CFA72E7D137F643B` FOREIGN KEY (`ristorante_id`) REFERENCES `ristorante` (`id`),
  ADD CONSTRAINT `FK_CFA72E7DBD2D3B3D` FOREIGN KEY (`codOrdine`) REFERENCES `ordine` (`codOrdine`);

--
-- Limiti per la tabella `sezione`
--
ALTER TABLE `sezione`
  ADD CONSTRAINT `FK_68B49425CCD7E912` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`id`);

--
-- Limiti per la tabella `tariffario`
--
ALTER TABLE `tariffario`
  ADD CONSTRAINT `FK_D269A97DBCB7B459` FOREIGN KEY (`codRistorante`) REFERENCES `ristorante` (`id`);

--
-- Limiti per la tabella `tavolo`
--
ALTER TABLE `tavolo`
  ADD CONSTRAINT `FK_9CB99BB6137F643B` FOREIGN KEY (`ristorante_id`) REFERENCES `ristorante` (`id`);

--
-- Limiti per la tabella `turno`
--
ALTER TABLE `turno`
  ADD CONSTRAINT `FK_E79767622252EAF6BCB7B459` FOREIGN KEY (`giorno`,`codRistorante`) REFERENCES `giorno_apertura` (`giorno`, `ristorante_id`),
  ADD CONSTRAINT `FK_E797676272BFF3BF` FOREIGN KEY (`codDipendente`) REFERENCES `dipendente` (`codDipendente`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
